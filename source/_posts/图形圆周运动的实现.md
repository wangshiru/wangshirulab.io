---
title: 图形圆周运动的实现
date: 2019-11-28 14:04:10
tags: [百度地图]
---
# 动效分析

“电”字所在的圆圈，需要一个点和一个圆弧不停地做圆周运动动效，思路和上一期的曲线上的流动效果类似，只需要求出每一帧各个图形的位置，然后不断重绘即可。

# 小球位置计算

小球的位置永远在大圆上，可根据大圆的坐标位置来计算点的位置，Px = X+R*cos(angel),Py同理。通过更改角度来计算每一帧小球的位置坐标。

```javascript
var angel = 0
var pixel = map.pointToPixel(new BMap.Point(lng,lat))
var pointx = 10*Math.cos((angel+1)*Math.PI)+pixel.x
var pointy = 10*Math.sin((angel+1)*Math.PI)+pixel.y
```

# 圆弧绘制

圆弧的起始位置为每帧小球的其实位置，重绘频率与小球保持一致即可。需要注意的是，初始位置时，圆弧的角度为（-PI，0），而后每次绘制，角度的增减同小球保持一致。

```javascript
ctx.arc(pixel.x,pixel.y,10,angel*Math.PI,(angel-1)*Math.PI)
ctx.stroke()
```

写一个定时器，循环重绘每帧位置，即可实现动画效果。

# 注意

1. 圆弧起止角度的设置要跟随小球，特别是初始位置。
2. 每次设置循环定时器都要记得在适当的时候关闭，以免造成无法控制的局面。