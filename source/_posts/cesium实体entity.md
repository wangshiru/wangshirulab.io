---
title: cesium实体entity
date: 2020-03-16 08:26:38
tags: [Cesium.js]
top: 2
---
[参考博客](https://www.jianshu.com/p/c641d0e5b7a1)

# 点表示全部站点

```js
var point = $.ajax({url:"source/station.json",async:false})
var pointData = JSON.parse(point.responseText)
console.log(pointData)

pointData.forEach( point => {
    viewer.entities.add({
        point : {
            pixelSize : 2,
            color : Cesium.Color.GREEN,
            material : new Cesium.PolylineArrowMaterialProperty(Cesium.Color.YELLOW)
        },
        position: Cesium.Cartesian3.fromDegrees(point.lng, point.lat),
    });
})
```

# 圆柱表示拓扑站点

```js
function draw_station(){
    for(var i=0;i<10;i++){
        viewer.entities.add({
            // 加载圆柱
            cylinder : {
                length : 400000.0,
                topRadius : 30000.0,
                bottomRadius : 30000.0,
                material : Cesium.Color.GREEN.withAlpha(0.8),
                outline : false,
                outlineColor : Cesium.Color.RED,
                heightReference: Cesium.HeightReference.CLAMP_TO_GROUND
            },
            position : Cesium.Cartesian3.fromDegrees(pointData[i].lng, pointData[i].lat,0.0)
        });
    }
}
```

# 线条

```js
function draw_lines(){
    for(var i=0;i<10;i++){
        viewer.entities.add({
            id:"line_"+i,
            polyline: {
                positions: Cesium.Cartesian3.fromDegreesArray([pointData[i].lng, pointData[i].lat,pointData[i+1].lng, pointData[i+1].lat]),
                width: 2,
                material: new Cesium.PolylineTrailLinkMaterialProperty(Cesium.Color.WHITE, 3000)
            }
        });
    }
}
```

## 线条流动效果

[参考代码](https://github.com/YanzheZhang/Cesium.HPUZYZ.Demo/blob/master/Cesium1.43/MyDemos/Tools-35TrailLine-flypath.html)

```js
/*
    流动纹理线
    color 颜色
    duration 持续时间 毫秒
*/
function PolylineTrailLinkMaterialProperty(color, duration) {
    this._definitionChanged = new Cesium.Event();
    this._color = undefined;
    this._colorSubscription = undefined;
    this.color = color;
    this.duration = duration;
    this._time = (new Date()).getTime();
}
Cesium.defineProperties(PolylineTrailLinkMaterialProperty.prototype, {
    isConstant: {
        get: function () {
            return false;
        }
    },
    definitionChanged: {
        get: function () {
            return this._definitionChanged;
        }
    },
    color: Cesium.createPropertyDescriptor('color')
});
PolylineTrailLinkMaterialProperty.prototype.getType = function (time) {
    return 'PolylineTrailLink';
}
PolylineTrailLinkMaterialProperty.prototype.getValue = function (time, result) {
    if (!Cesium.defined(result)) {
        result = {};
    }
    result.color = Cesium.Property.getValueOrClonedDefault(this._color, time, Cesium.Color.WHITE, result.color);
    result.image = Cesium.Material.PolylineTrailLinkImage;
    result.time = (((new Date()).getTime() - this._time) % this.duration) / this.duration;
    return result;
}
PolylineTrailLinkMaterialProperty.prototype.equals = function (other) {
    return this === other ||
        (other instanceof PolylineTrailLinkMaterialProperty &&
        Property.equals(this._color, other._color))
}

Cesium.PolylineTrailLinkMaterialProperty = PolylineTrailLinkMaterialProperty;
Cesium.Material.PolylineTrailLinkType = 'PolylineTrailLink';
Cesium.Material.PolylineTrailLinkImage = "../images/colors2.png";
Cesium.Material.PolylineTrailLinkSource = "czm_material czm_getMaterial(czm_materialInput materialInput)\n\
                                                {\n\
                                                    czm_material material = czm_getDefaultMaterial(materialInput);\n\
                                                    vec2 st = materialInput.st;\n\
                                                    vec4 colorImage = texture2D(image, vec2(fract(st.s - time), st.t));\n\
                                                    material.alpha = colorImage.a * color.a;\n\
                                                    material.diffuse = (colorImage.rgb+color.rgb)/2.0;\n\
                                                    return material;\n\
                                                }";
Cesium.Material._materialCache.addMaterial(Cesium.Material.PolylineTrailLinkType, {
    fabric: {
        type: Cesium.Material.PolylineTrailLinkType,
        uniforms: {
            color: new Cesium.Color(1.0, 0.0, 0.0, 0.5),
            image: Cesium.Material.PolylineTrailLinkImage,
            time: 0
        },
        source: Cesium.Material.PolylineTrailLinkSource
    },
    translucent: function (material) {
        return true;
    }
});

var m_white = new Cesium.PolylineTrailLinkMaterialProperty(Cesium.Color.WHITE, 1000)

for(var i=0;i<10;i++){
    entity = viewer.entities.add({
        name: 'PolylineTrail',
        polyline: {
            positions: Cesium.Cartesian3.fromDegreesArray([pointData[i].lng, pointData[i].lat,pointData[i+1].lng, pointData[i+1].lat]),
            width: 1,
            material: m_white
        }
    });
}
```

如果需要不同颜色的流动线，需要再创建多个PolylineTrailLinkMaterialProperty，不同命名，创建不同的颜色材质。