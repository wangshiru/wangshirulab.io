---
title: scala基础入门
date: 2020-12-11 10:32:43
tags: [大数据]
# top: True
---

[林子雨教材在线参考资料](http://dblab.xmu.edu.cn/post/spark)


# centos7的java安装

```bash
# 检查是否已安装
yum list installed |grep java
# 查看yum库中的Java安装包
yum -y list java*
# 以yum库中java-1.8.0为例, "*"表示将java-1.8.0的所有相关Java程序都安装上
yum -y install java-1.8.0-openjdk*
# 查看java版本
java -version
```

# scala安装

```bash
# 切换目录
cd /usr/local
# 下载Scala2.11
wget https://downloads.lightbend.com/scala/2.11.8/scala-2.11.8.tgz
# 解压
tar -xzvf scala-2.11.8.tgz
# 重命名文件夹
mv scala scala-2.11.8
# 进入根目录配置全局变量
cd ~
vim .bash_profile
# 添加如下配置
export SCALA_HOME=/usr/local/scala
export PATH=$PATH:$SCALA_HOME/bin
# 输入以下命令使环境变量快速生效
source .bash_profile
# 输入scala进入
scala
Welcome to Scala 2.11.8 (OpenJDK 64-Bit Server VM, Java 1.8.0_272).
Type in expressions for evaluation. Or try :help.

scala> 
```

# scala简易入门

建立一个文件夹存放scala文件，并创建文件 `demo1.scala`

```
println("this is first line")
println("this is second line")
```

进入scala，执行 `:load /usr/local/scalapro/mycode/demo1.scala`

```bash
scala> :load /usr/local/scalapro/mycode/demo1.scala
Loading /usr/local/scalapro/mycode/demo1.scala...
this is first line
this is second line
```

退出，新建 `helloworld.scala` 

```
object helloworld{
    def main(args:Array[String]){
        println("hello world")
    }
}
```

执行 `scala helloworld.scala ` 即可输出 `helloworld`
执行 `scalac helloworld.scala ` 可生成2个文件 `helloworld.class  helloworld$.class`，前者为可在java虚拟机上运行的字节码文件，然后使用命令 ` scala -classpath . helloworld` 运行该程序


# scala基础知识
