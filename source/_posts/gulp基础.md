---
title: gulp基础
date: 2021-04-08 13:59:17
tags: [工具]
---

# 安装

```bash
# 初始化项目
npm init -y
# 安装
npm install -g/-D gulp
```

package.json

```json
"scripts": {
    "build": "gulp",
}
```

# 配置

gulpfile.js

```js
// 压缩js uglifyjs
function js(){
    console.log('this is js task')
}
// 对scss/less编译、压缩，输出css文件
function css(){
    console.log('this is css task')
}
// 监听这些文件的变化
function watcher(){
    
}
// 删除dist目录中的内容
function clean(){
    
}

exports.scripts = js
exports.styles = css

exports.default = function(){
    console.log('hello gulp!')
}
```

调用任务

```
npx gulp --tasks
npx gulp scripts
```

# 插件

安装

```bash
yarn add gulp-sass gulp-autoprefixer gulp-load-plugins gulp-uglify del -D
```

gulpfile.js

```js
const { src, dest, series, watch } = require('gulp')
// gulp-uglify=>plugins.uglify = require('gulp-uglify')
// const plugins = require('gulp-load-plugins')
const uglify = require('gulp-uglify')
const del = require('del')
const autoprefixer = require('gulp-autoprefixer')
const sass = require('gulp-sass')

// 压缩js uglifyjs
function js(cb){
    console.log('this is js task')
    src('js/*.js')
    // 下一个处理环节
        .pipe(uglify())
        .pipe(dest('./dist/js'))
    cb()
}
// 对scss/less编译、压缩，输出css文件
function css(cb){
    console.log('this is css task')
    src('css/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefixer({
            cascade: false,
            remove: false
        }))
        .pipe(dest('./dist/css'))
    cb()
}
// 监听这些文件的变化
function watcher(cb){
    watch('js/*.js', js)
    watch('css/*.scss', css)
    cb()
}
// 删除dist目录中的内容
function clean(cb){
    del('./dist')
    cb()
}

exports.scripts = js
exports.styles = css
exports.clean = clean

// 自动编译更新
exports.default = series([
    clean,
    js,
    css,
    watcher
])
```

# browsersync 网页实时刷新

安装

```bash
yarn add browser-sync -D
```

gulpfile.js

```js
const { src, dest, series, watch } = require('gulp')
const uglify = require('gulp-uglify')
const del = require('del')
const autoprefixer = require('gulp-autoprefixer')
const sass = require('gulp-sass')
const browserSync = require('browser-sync').create()
const reload = browserSync.reload

// 压缩js uglifyjs
function js(cb){
    console.log('this is js task')
    src('js/*.js')
    // 下一个处理环节
        .pipe(uglify())
        .pipe(dest('./dist/js'))
        // reload
        .pipe(reload({ stream: true }))
    cb()
}
// 对scss/less编译、压缩，输出css文件
function css(cb){
    console.log('this is css task')
    src('css/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefixer({
            cascade: false,
            remove: false
        }))
        .pipe(dest('./dist/css'))
        // reload
        .pipe(reload({ stream: true }))
    cb()
}
// 监听这些文件的变化
function watcher(cb){
    watch('js/*.js', js)
    watch('css/*.scss', css)
    cb()
}
// 删除dist目录中的内容
function clean(cb){
    del('./dist')
    cb()
}

// server任务
function server(cb){
    browserSync.init({
        server: {
            baseDir: './'
        }
    })
    cb()
}

exports.scripts = js
exports.styles = css
exports.clean = clean

exports.default = series([
    clean,
    js,
    css,
    server,
    watcher
])
```

执行 npm run build 即可