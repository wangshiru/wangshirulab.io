---
title: linux基础
date: 2020-06-20 09:17:07
tags: [Linux]
---
# 访问服务器地址

```bash
ssh root@192.168.0.21
```

# 基础文档操作

```bash
# 创建文件夹
mkdir abc
# 查看文件夹下的目录
ll abc
# 删除文件夹
rm -r abc
# 强制删除文件夹（危险）
rm -rf abc
# 删除文件
rm index.html
# 修改文件名
mv index.html index1.html
# 移动文件
mv index.html ../index.html
# 拷贝
cp a.js a1.js
# 新建文件
touch b.js
# 创建并打开文件
vi a.js
# 查看文件内容
cat a.js
head a.js
tail a.js
# 在文件中查找关键字
grep '关键字' a.js
```

# 下载

```bash
# 下载
wget 文件地址
# 解压
tar zxvf 文件名
# 压缩
tar zcvf 目标文件名 源文件名
# 查看进程
ps -ef | grep docker
# grep搜索某个进程
# 杀死某个进程
kill -9 进程id
# 查看系统服务
service sshd status
# 关闭服务
service sshd stop
# 重启服务
service sshd restart
# 查看系统服务
systemctl status firewalld.service
```

# 防火墙

[防火墙操作参考](https://www.cnblogs.com/yizhipanghu/p/11171211.html)

```bash
# 查看防火墙状态
firewall-cmd --state
# 查看防火墙放行端口
firewall-cmd --list-port
# 添加防火墙放行端口
# –zone 作用域
# –add-port=80/tcp 添加端口，格式为：端口/通讯协议
#–permanent 永久生效，没有此参数重启后失效
firewall-cmd --zone=public --add-port=8081/tcp --permanent
# 放行后重载防火墙
firewall-cmd --reload
```

# docker

[docker常用操作](https://www.cnblogs.com/mrlwc/p/12083830.html)

```bash
# 删除镜像
docker rmi 镜像id
# 删除容器
docker rm 容器id
# 查看正在运行的容器
docker ps 
# 查看全部容器，含已退出的容器
docker ps -a
# 停止容器
docker stop 容器id
# 启动一个已停止的容器
docker start 容器ID或容器名
```