---
title: 电网demo技术（一）
date: 2019-07-15 09:30:53
tags: [百度地图]
---

# 接口使用

1. 设置地图中心点（随点击标注而变化）
2. 设置地图允许的最小/大级别
3. 设置点的新图标（按type分类）
4. 添加信息图标
5. 给标注绑定事件（瓦片加载、滑动、点击）
6. 具体技术参考 百度地图 官方API，用法已经比较详细

# 标注和潮流线

1. 标注使用自定义图标，三种类型
2. 潮流线需要表示流向和大小，使用 Mapv （baidu-map-forceEdgeBundling）展示，三种类型

# DataSet

```javascript
var pointJson = [
 	{lng: 112.19678, id: "57", type: "normal", lat: 30.9385, info: "湖北荆门变电站"},
 	{lng: 118.39869, id: "13", type: "warning", lat: 31.35427, info: "安徽芜湖站"},
 	{lng: 117.26289, id: "14", type: "danger", lat: 31.834887, info: "四川锦西电厂"}
]
var lineJson = [
 	{
 		"sourceid":"0",
 		"targetid":"1",
 		"type": 'normal'
 	},
 	{
 		"sourceid":"1",
 		"targetid":"2",
 		"type": 'warning'
 	},
 	{
 		"sourceid":"2",
 		"targetid":"3",
 		"type": 'danger'
 	}
]
```

# 标注渲染

三种类型标注的渲染，注意每创建一个标注，都需要给该标注绑定事件

```javascript
function addMarkerInfo(markerInfo){
    for(i=0;i<markerInfo.length;i++){
        if(markerInfo[i].type=='normal'){
            var normalMyIcon = new BMap.Icon("../assets/img/mark/normal/green.png", new BMap.Size(37,37));
            var normalMarker = new BMap.Marker(new BMap.Point(markerInfo[i].lng,markerInfo[i].lat),{icon:normalMyIcon});  // 创建normal标注
            map.addOverlay(normalMarker); 
            if($("#regionLevel").hasClass("active")){
                addMouseoverHandler(markerInfo[i].id,normalMarker); 
                
            }else if($("#stationLevel").hasClass("active")){
                addMouseoverHandler(markerInfo[i].id,normalMarker); 
                addClickHandler(markerInfo[i].id,normalMarker); 
            }
        }else if(markerInfo[i].type=='warning'){
            var warningMyIcon = new BMap.Icon("../assets/img/mark/warning/yellow.png", new BMap.Size(37,37));
            var warningMarker = new BMap.Marker(new BMap.Point(markerInfo[i].lng,markerInfo[i].lat),{icon:warningMyIcon});  // 创建warning标注
            map.addOverlay(warningMarker); 
            if($("#regionLevel").hasClass("active")){
                addMouseoverHandler(markerInfo[i].id,warningMarker); 
                
            }else if($("#stationLevel").hasClass("active")){
                addMouseoverHandler(markerInfo[i].id,warningMarker); 
                addClickHandler(markerInfo[i].id,warningMarker); 
            }
        }else if(markerInfo[i].type=='danger'){
            var dangerMyIcon = new BMap.Icon("../assets/img/mark/danger/red.png", new BMap.Size(37,37));
            var dangerMarker = new BMap.Marker(new BMap.Point(markerInfo[i].lng,markerInfo[i].lat),{icon:dangerMyIcon});  // 创建danger标注
            map.addOverlay(dangerMarker); 
            if($("#regionLevel").hasClass("active")){
                addMouseoverHandler(markerInfo[i].id,dangerMarker); 
                
            }else if($("#stationLevel").hasClass("active")){
                addMouseoverHandler(markerInfo[i].id,dangerMarker); 
                addClickHandler(markerInfo[i].id,dangerMarker); 
            }
        }
        //addClickHandler(markerInfo[i].id,marker2);   
    }
}
```

# 潮流线渲染

流线渲染思路：一个点图层（标注已经用百度地图标好，这里只是确定位置），三个线图层（正常、警告、危险），根据类型判断线应该放在哪个类型的集合中，分别配置不同类型的参数.

```javascript
function loadMapV(pointJson,lineJson){
    var node_data = {}
    var normal_edge_data = []
    var warning_edge_data = []
    var danger_edge_data = []
    for(i in pointJson){
        var key = pointJson[i].id  
        node_data[key] = {
            x: parseFloat(pointJson[i].lng),
            y: parseFloat(pointJson[i].lat),
        }
    }
    for(j in lineJson){
        if(lineJson[j].type=='normal'){
            normal_edge_data.push(
                {"source": lineJson[j].sourceid, "target": lineJson[j].targetid}
            )
        }else if(lineJson[j].type=='warning'){
            warning_edge_data.push(
                {"source": lineJson[j].sourceid, "target": lineJson[j].targetid}
            )
        }else if(lineJson[j].type=='danger'){
            danger_edge_data.push(
                {"source": lineJson[j].sourceid, "target": lineJson[j].targetid}
            )
        }
    }
    console.log(node_data)
    console.log(normal_edge_data)
    var normal_fbundling = mapv.utilForceEdgeBundling()
                    .nodes(node_data)
                    .edges(normal_edge_data);
    var normal_results = normal_fbundling();  

    var warning_fbundling = mapv.utilForceEdgeBundling()
                    .nodes(node_data)
                    .edges(warning_edge_data);
    var warning_results = warning_fbundling(); 
    var danger_fbundling = mapv.utilForceEdgeBundling()
                    .nodes(node_data)
                    .edges(danger_edge_data);
    var danger_results = danger_fbundling(); 

    var normal_data = [];
    var normal_timeData = [];

    var warning_data = [];
    var warning_timeData = [];

    var danger_data = [];
    var danger_timeData = [];
    
    for (var i = 0; i < normal_results.length; i++) {
        var normal_line = normal_results[i];
        var normal_coordinates = [];
        for (var j = 0; j < normal_line.length; j++) {
            normal_coordinates.push([normal_line[j].x, normal_line[j].y]);
            normal_timeData.push({
                geometry: {
                    type: 'Point',
                    coordinates: [normal_line[j].x, normal_line[j].y]
                },
                count: 1,
                time: j
            });
        }
        normal_data.push({
            geometry: {
                type: 'LineString',
                coordinates: normal_coordinates
            }
        });
    }
    for (var i = 0; i < warning_results.length; i++) {
        var warning_line = warning_results[i];
        var warning_coordinates = [];
        for (var j = 0; j < warning_line.length; j++) {
            warning_coordinates.push([warning_line[j].x, warning_line[j].y]);
            warning_timeData.push({
                geometry: {
                    type: 'Point',
                    coordinates: [warning_line[j].x, warning_line[j].y]
                },
                count: 1,
                time: j
            });
        }
        warning_data.push({
            geometry: {
                type: 'LineString',
                coordinates: warning_coordinates
            }
        });
    }
    for (var i = 0; i < danger_results.length; i++) {
        var danger_line = danger_results[i];
        var danger_coordinates = [];
        for (var j = 0; j < danger_line.length; j++) {
            danger_coordinates.push([danger_line[j].x, danger_line[j].y]);
            danger_timeData.push({
                geometry: {
                    type: 'Point',
                    coordinates: [danger_line[j].x, danger_line[j].y]
                },
                count: 1,
                time: j
            });
        }
        danger_data.push({
            geometry: {
                type: 'LineString',
                coordinates: danger_coordinates
            }
        });
    }
    var normal_dataSet = new mapv.DataSet(normal_data);
    var warning_dataSet = new mapv.DataSet(warning_data);
    var danger_dataSet = new mapv.DataSet(danger_data);
    
    var options = {
        strokeStyle: 'rgba(47, 140, 141, 0.3)',
        draw: 'simple'
    }
    
    var mapvLayer = new mapv.baiduMapLayer(map, normal_dataSet, options);
    var mapvLayer = new mapv.baiduMapLayer(map, warning_dataSet, options);
    var mapvLayer = new mapv.baiduMapLayer(map, danger_dataSet, options);
    
    var n_dataSet = new mapv.DataSet(normal_timeData);
    var w_dataSet = new mapv.DataSet(warning_timeData);
    var d_dataSet = new mapv.DataSet(danger_timeData);
    
    var n_options = {
        fillStyle: 'rgba(255, 250, 250, 0.9)',
        globalCompositeOperation: 'lighter',
        size: 2,
        animation: {
            type: 'time',
            stepsRange: {
                start: 0,
                end: 100
            },
            trails: 2,
            duration: 5,
        },
        draw: 'simple'
    }
    var mapvLayer = new mapv.baiduMapLayer(map, n_dataSet, n_options);

    var w_options = {
        fillStyle: 'rgba(167, 136, 5, 0.9)',
        globalCompositeOperation: 'lighter',
        size: 2,
        animation: {
            type: 'time',
            stepsRange: {
                start: 0,
                end: 100
            },
            trails: 2,
            duration: 4,
        },
        draw: 'simple'
    }
    
    var mapvLayer = new mapv.baiduMapLayer(map, w_dataSet, w_options);

    var d_options = {
        fillStyle: 'rgba(149, 41, 17, 0.9)',
        globalCompositeOperation: 'lighter',
        size: 3,
        animation: {
            type: 'time',
            stepsRange: {
                start: 0,
                end: 100
            },
            trails: 2,
            duration: 3,
        },
        draw: 'simple'
    }
    var mapvLayer = new mapv.baiduMapLayer(map, d_dataSet, d_options);
}
```

# 小结

* 多次利用百度地图的API进行事件的绑定，实现弹窗和不同级别的操作控制。在站点级别和区域级别的切换中，重新渲染点线，这种方式有待改善。
* ajax轮询的方式需要改为websocket实时，减少服务器压力。