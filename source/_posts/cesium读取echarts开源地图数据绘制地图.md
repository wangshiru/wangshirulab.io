---
title: cesium读取echarts开源地图数据绘制地图
date: 2020-03-16 11:06:14
tags: [Cesium.js]
top: 2
---

# 绘制地图，全国和区域   

```js
// 六大区域名称
var areas_name = [
    { name:'东北', cp:{'lng':123.307279,'lat':46.375662} },
    { name:'华北', cp:{'lng':115.433236,'lat':41.707946} },
    { name:'西北', cp:{'lng':96.888761,'lat':39.120975} },
    { name:'西南', cp:{'lng':98.80208,'lat':31.2172815} },
    { name:'华中', cp:{'lng':113.814274,'lat':29.236317} },
    { name:'华东', cp:{'lng':118.523982,'lat':31.784774} }
]

var level = '全国'

// 绘制地图，全国和区域
function draw_china_test(provincesNames){
    var china = $.ajax({url:"source/province/china.json",async:false})
    chinaData = decode(JSON.parse(china.responseText))
    var china = chinaData.features
    if(provincesNames == '全国'){
        var chinajson = $.ajax({url:"source/chinaborder.json",async:false});
        var china_border_json = JSON.parse(chinajson.responseText);
        var arr = []
        for(i in areas_name){
            viewer.entities.add({
                id: areas_name[i].name,
                position : Cesium.Cartesian3.fromDegrees(areas_name[i].cp.lng, areas_name[i].cp.lat),
                label : {
                    text : areas_name[i].name,
                    font : '18px Helvetica',
                    fillColor : Cesium.Color.WHITE,
                }
            });
        }
        
        for(var i in china_border_json){
            arr = []
            for(j in china_border_json[i]){
                arr.push(china_border_json[i][j].lng)
                arr.push(china_border_json[i][j].lat)
            }
            viewer.entities.add({
                polyline : {
                    id: "china_polyline"+i,
                    positions : Cesium.Cartesian3.fromDegreesArray(arr),
                    width : 1,
                    material : new Cesium.PolylineOutlineMaterialProperty({
                        color : Cesium.Color.YELLOW,
                        outlineWidth : 0,
                        outlineColor : Cesium.Color.BLACK,
                    }),
                    clampToGround : true
                }
            });
        }

        provinces_Names_HB = ['北京','天津','河北','山西','山东','内蒙古']
        provinces_Names_HD = ['上海','江苏','浙江','安徽','福建']
        provinces_Names_DB = ['黑龙江','辽宁','吉林']
        provinces_Names_HZ = ['湖北','湖南','河南','江西']
        provinces_Names_XB = ['陕西','甘肃','青海','宁夏','新疆']
        provinces_Names_XN = ['四川','重庆','西藏']

        china.forEach( provinces => {
            var color = ''
            if($.inArray(provinces.properties.name , provinces_Names_HB) != -1){
                color = 'SIENNA'
            }else if($.inArray(provinces.properties.name , provinces_Names_HD) != -1){
                color = 'GREEN'
            }else if($.inArray(provinces.properties.name , provinces_Names_DB) != -1){
                color = 'RED'
            }else if($.inArray(provinces.properties.name , provinces_Names_HZ) != -1){
                color = 'ROYALBLUE'
            }else if($.inArray(provinces.properties.name , provinces_Names_XB) != -1){
                color = 'GOLDENROD'
            }else if($.inArray(provinces.properties.name , provinces_Names_XN) != -1){
                color = 'GREENYELLOW'
            }else{
                color = 'SKYBLUE'
            }
            var province = provinces.geometry
            var areas = province.coordinates
            areas.forEach( (points) => {
                if(points[0][0] instanceof Array){
                    points.forEach(point=>{
                        draw_china_area(point,color)
                    })
                }else{
                    draw_china_area(points,color)
                }
            })
        })
        add_china_clickHandler()
        viewer.zoomTo(viewer.entities);
    }else{
        china.forEach( provinces => {
            if($.inArray(provinces.properties.name , provincesNames) != -1){
                var province = provinces.geometry
                var areas = province.coordinates
                viewer.entities.add({
                    id: 'province_name='+provinces.properties.name,
                    position : Cesium.Cartesian3.fromDegrees(provinces.properties.cp[0], provinces.properties.cp[1]),
                    label : {
                        text : provinces.properties.name,
                        font : '18px Helvetica',
                        fillColor : Cesium.Color.WHITE,
                    }
                });
                areas.forEach( (points) => {
                    if(points[0][0] instanceof Array){
                        points.forEach(point=>{
                            draw_china_area(point)
                        })
                    }else{
                        draw_china_area(points)
                    }
                })
            }
        })
        add_area_clickHandler()
        viewer.zoomTo(viewer.entities);
    }
}
```

# 读china.json

```js
function draw_china_area(points,color){
    var area_color = Cesium.Color.GREEN.withAlpha(0.5)
    if(color == 'SIENNA'){
        area_color = Cesium.Color.SIENNA.withAlpha(0.5)
    }else if(color == 'GREENYELLOW'){
        area_color = Cesium.Color.GREENYELLOW.withAlpha(0.5)
    }else if(color == 'RED'){
        area_color = Cesium.Color.RED.withAlpha(0.5)
    }else if(color == 'GOLDENROD'){
        area_color = Cesium.Color.GOLDENROD.withAlpha(0.5)
    }else if(color == 'SKYBLUE'){
        area_color = Cesium.Color.SKYBLUE.withAlpha(0.5)
    }else if(color == 'ROYALBLUE'){
        area_color = Cesium.Color.ROYALBLUE.withAlpha(0.5)
    }
    var china_array = []
    for(i in points){
        china_array.push(points[i][0])
        china_array.push(points[i][1])
    }
    
    viewer.entities.add({
        polygon : {
            hierarchy : new Cesium.PolygonHierarchy(Cesium.Cartesian3.fromDegreesArray(china_array)),
            material : area_color,
            // classificationType : Cesium.ClassificationType.BOTH
        }
    });
    viewer.entities.add({
        polyline : {
            positions : Cesium.Cartesian3.fromDegreesArray(china_array),
            width : 1.5,
            material : new Cesium.PolylineOutlineMaterialProperty({
                color : Cesium.Color.WHITE.withAlpha(0.1),
                outlineWidth : 0,
                outlineColor : Cesium.Color.BLACK
            }),
            clampToGround : true
        }
    });
}
```

# 读取某个省文件

```js
function get_province_json(province){
    var province = $.ajax({url:"source/province/"+province+'.json',async:false})
    provinceJson = decode(JSON.parse(province.responseText))
    provinceJson.features.forEach(areas => {
        viewer.entities.add({
            id: 'city_name='+areas.properties.name,
            position : Cesium.Cartesian3.fromDegrees(areas.properties.cp[0], areas.properties.cp[1]),
            label : {
                text : areas.properties.name,
                font : '14px Helvetica',
                fillColor : Cesium.Color.WHITE,
            }
        });
        areas.geometry.coordinates.forEach( (points) => {
            if(points[0][0] instanceof Array){
                points.forEach(point=>{
                    drawcity(point)
                })
            }else{
                drawcity(points)
            }
        })
    })
    viewer.zoomTo(viewer.entities);
    add_province_clickHandler()
}
```

# 某个省文件中的区块绘制

```js
function drawcity(points){
    var points_array = []
    for(i in points){
        points_array.push(points[i][0])
        points_array.push(points[i][1])
    }
    // 添加区域覆盖范围和连线
}
```

# 添加区域覆盖范围和连线

```js
viewer.entities.add({
    polygon : {
        hierarchy : new Cesium.PolygonHierarchy(Cesium.Cartesian3.fromDegreesArray(points_array)),
        material : Cesium.Color.GREEN.withAlpha(0.5)
    }
});
viewer.entities.add({
    polyline : {
        positions : Cesium.Cartesian3.fromDegreesArray(points_array),
        width : 1.5,
        material : new Cesium.PolylineOutlineMaterialProperty({
            color : Cesium.Color.WHITE.withAlpha(0.1),
            outlineWidth : 0,
            outlineColor : Cesium.Color.BLACK
        })
    }
});
```

# 地图点击文字进行层级跳转

```js
// 全国地图文字绑定点击事件
function add_china_clickHandler(){
    var handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
    handler.setInputAction(function(click) {
        var pick = viewer.scene.pick(click.position);
        var provinces = []
        if(Cesium.defined(pick) && (pick.id.id === '华北')){
            provinces = ['北京','天津','河北','山西','山东','内蒙古']
            viewer.entities.removeAll();
            draw_china_test(provinces)
            level = 'area'
        }else if(Cesium.defined(pick) && (pick.id.id === '华东')){
            provinces = ['上海','江苏','浙江','安徽','福建']
            viewer.entities.removeAll(); 
            draw_china_test(provinces)
            level = 'area'
        }else if(Cesium.defined(pick) && (pick.id.id === '东北')){
            provinces = ['黑龙江','辽宁','吉林']
            viewer.entities.removeAll(); 
            draw_china_test(provinces)
            level = 'area'
        }else if(Cesium.defined(pick) && (pick.id.id === '华中')){
            provinces = ['湖北','湖南','河南','江西']
            viewer.entities.removeAll(); 
            draw_china_test(provinces)
            level = 'area'
        }else if(Cesium.defined(pick) && (pick.id.id === '西北')){
            provinces = ['陕西','甘肃','青海','宁夏','新疆']
            viewer.entities.removeAll(); 
            draw_china_test(provinces)
            level = 'area'
        }else if(Cesium.defined(pick) && (pick.id.id === '西南')){
            provinces = ['四川','重庆','西藏']
            viewer.entities.removeAll();
            draw_china_test(provinces)
            level = 'area'
        }
    },Cesium.ScreenSpaceEventType.LEFT_CLICK);
}

// 某区域文字绑定点击事件
function add_area_clickHandler(){
    var handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
    handler.setInputAction(function(click) {
        var pick = viewer.scene.pick(click.position);
        if(Cesium.defined(pick) && (pick.id.id.includes('province_name=') )){
            console.log(pick.id.id)
            var province_name = pick.id.id.substr(14)
            // viewer.entities.removeAll()
            viewer.entities.removeAll();
            get_province_json(province_name)
            level = 'province'
        }
    },Cesium.ScreenSpaceEventType.LEFT_CLICK)
}

function add_province_clickHandler(){
    var handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
    handler.setInputAction(function(click) {
        var pick = viewer.scene.pick(click.position);
        if(Cesium.defined(pick) && (pick.id.id.includes('city_name=') )){
            console.log(pick.id.id)
            var city_name = pick.id.id.substr(10)
            // viewer.entities.removeAll()
            viewer.entities.removeAll();
            // get_province_json(city_name)
            provinceJson.features.forEach(areas => {
                if(areas.properties.name == city_name){
                    viewer.entities.add({
                        id: 'city_name='+areas.properties.name,
                        position : Cesium.Cartesian3.fromDegrees(areas.properties.cp[0], areas.properties.cp[1]),
                        label : {
                            text : areas.properties.name,
                            font : '14px Helvetica',
                            fillColor : Cesium.Color.WHITE,
                        }
                    });
                    areas.geometry.coordinates.forEach( (points) => {
                        if(points[0][0] instanceof Array){
                            points.forEach(point=>{
                                drawcity(point)
                            })
                        }else{
                            drawcity(points)
                        }
                    })
                }
            })
            viewer.zoomTo(viewer.entities);
            level = 'city'
        }
    },Cesium.ScreenSpaceEventType.LEFT_CLICK)
}

```

# 调用绘制全国地图方法

```js
// 绘制全国地图
draw_china_test('全国')
```

省文件的数据格式解析比较复杂，解密算法是从echarts.js的源文件中找到的，作为库文件引入，内容如下util.js。

# util.js

```js
function decode(json) {
    if (!json.UTF8Encoding) {
        return json;
    }
    var encodeScale = json.UTF8Scale;
    if (encodeScale == null) {
        encodeScale = 1024;
    }

    var features = json.features;

    for (var f = 0; f < features.length; f++) {
        var feature = features[f];
        var geometry = feature.geometry;
        var coordinates = geometry.coordinates;
        var encodeOffsets = geometry.encodeOffsets;

        for (var c = 0; c < coordinates.length; c++) {
            var coordinate = coordinates[c];

            if (geometry.type === 'Polygon') {
                coordinates[c] = decodePolygon(
                    coordinate,
                    encodeOffsets[c],
                    encodeScale
                );
            }
            else if (geometry.type === 'MultiPolygon') {
                for (var c2 = 0; c2 < coordinate.length; c2++) {
                    var polygon = coordinate[c2];
                    coordinate[c2] = decodePolygon(
                        polygon,
                        encodeOffsets[c][c2],
                        encodeScale
                    );
                }
            }
        }
    }
    // Has been decoded
    json.UTF8Encoding = false;
    return json;
}

function decodePolygon(coordinate, encodeOffsets, encodeScale) {
    var result = [];
    var prevX = encodeOffsets[0];
    var prevY = encodeOffsets[1];

    for (var i = 0; i < coordinate.length; i += 2) {
        var x = coordinate.charCodeAt(i) - 64;
        var y = coordinate.charCodeAt(i + 1) - 64;
        // ZigZag decoding
        x = (x >> 1) ^ (-(x & 1));
        y = (y >> 1) ^ (-(y & 1));
        // Delta deocding
        x += prevX;
        y += prevY;

        prevX = x;
        prevY = y;
        // Dequantize
        result.push([x / encodeScale, y / encodeScale]);
    }

    return result;
}
```
