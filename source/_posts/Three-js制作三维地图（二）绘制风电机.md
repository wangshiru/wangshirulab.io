---
title: Three.js制作三维地图（二）绘制风电机
date: 2020-01-14 15:58:02
tags: [Three.js]
top: 2
---

# 绘制风电机模型

风电机模型的难点主要是旋转。绘制风电机有2中方式，一种是网格，一种是形状拼接。
使用网格的方式，在旋转的时候出现问题。旋转默认的旋转原点为原点，即（0，0，0）点。
使用拼接的方式，旋转仍然有问题，绕每个形状的中心进行旋转，也不满足要求。
最后的解决方法为在形状拼接的时候，将每个圆锥放在一个透明的正方体中，旋转正方体即可达到想要的效果

```js
function draw_chinaWindPowerStation(){
    chinaWindPowerStationConeGroup.children = []
    chinaWindPowerStationCylinderGroup.children = []
    for(var i = 0; i < chinaVector3WindPowerStation.length; i++){
        var r = 0.3
        var l = 4
        var h = 10
        var x = chinaVector3WindPowerStation[i][0]
        var y = chinaVector3WindPowerStation[i][1]

        // 正方体
        var geometry = new THREE.BoxGeometry( 1, 1, 1 );
        var material = new THREE.MeshStandardMaterial( {color: "#fff", transparent: true, opacity: 0} );
        var cube1 = new THREE.Mesh( geometry, material )
        cube1.position.x = x
        cube1.position.y = y
        cube1.position.z = 6.5+h

        // 圆锥
        var geometry = new THREE.ConeBufferGeometry( r, l, 32 );
        var material = new THREE.MeshStandardMaterial( {color: "#fff"} );
        var cone = new THREE.Mesh( geometry, material )
        cone.position.set(0,l/2,0)
        cone.castShadow = true
        cone.receiveShadow = true

        // 将圆锥加入到正方体中，旋转正方体时，圆锥一起旋转
        cube1.add(cone)
        cube1.rotation.z = 0
        cube1.rotation.y = Math.PI/2
        var cube2 = cube1.clone()
        cube2.rotation.z = Math.PI*2/3
        var cube3 = cube1.clone()
        cube3.rotation.z = Math.PI*4/3
        chinaWindPowerStationConeGroup.add(cube1)
        chinaWindPowerStationConeGroup.add(cube2)
        chinaWindPowerStationConeGroup.add(cube3)
        var geometry = new THREE.CylinderGeometry( r/2, r, h, 32 )
        var material = new THREE.MeshStandardMaterial( {color: "#fff"} )
        var cylinder = new THREE.Mesh( geometry, material )
        cylinder.position.set(x,y,6.5+h/2)
        cylinder.rotation.z = Math.PI/2
        cylinder.rotation.y = Math.PI/2
        chinaWindPowerStationCylinderGroup.add(cylinder)
    }
    scene.add(chinaWindPowerStationConeGroup)
    scene.add(chinaWindPowerStationCylinderGroup)
}
```

# 风扇旋转

```js
chinawindpoweranimate =  _.throttle(()=>{
    chinaWindPowerStationConeGroup.children.forEach(cube=>{
        cube.rotation.z += 0.01
    })
},10)
```