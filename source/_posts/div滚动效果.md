---
title: div滚动效果
date: 2020-05-30 17:22:33
tags: [效果]
---
# 绝对定位的div向上滚动

```js
function start_scroll(){
    var t = 0
    var interval = setInterval(() => {
        t = t-50
        var top = t+'px'
        console.log(top)
        $(".q_scroll").css('top',top)
    }, 1000)
}
```