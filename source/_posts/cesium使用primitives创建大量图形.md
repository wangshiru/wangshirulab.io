---
title: cesium使用primitives创建大量图形
date: 2022-01-24 11:07:51
tags: [Cesium.js]
top: 2
---

使用entity创建大量图形并实时更新渲染会导致页面卡顿，使用primitives可提高效率。

[参考博客1](https://xiaozhuanlan.com/topic/3092417685)
[参考博客2](https://xiaozhuanlan.com/topic/2410678359)
[参考博客3](https://xiaozhuanlan.com/topic/1485630279)


# 区别

## entity
1. 绘制大量静态图形时性能差
2. 几何形状和外观耦合在一起

## primitives
1. Geometry + Appearance
2. 绘制组合图形，减少 CPU 的开销，更充分利用 GPU
3. 将Geometry （几何）和 Appearance （外观）解耦，可以分别修改


# 代码

1. 创建全局viewer，通过节点位置信息和连线信息创建节点和线的primitives数组，合并图形以提高渲染效率。
2. 包含自定义material的创建，使用fabric创建纹理材质。
3. 数据更新后根据id获取primitives并修改颜色。

```vue
<template>
    <div class="main">
        <div id="cesiumContainer"></div>
        <div class="btn">
            <van-button :type="type == 1 ? 'primary' : 'default'" @click="changeType(1, $event)" style="float: left">按钮1</van-button>
            <van-button :type="type == 2 ? 'primary' : 'default'" @click="changeType(2, $event)" style="float: right">按钮2</van-button>
        </div>
    </div>
    
</template>

<script> 
let viewer = null
let nodesPosition = null
let labels = null
let nodesInstances = []
let GInstances = []
let LoadInstances = []
let linesInstances = []
let websocketMap = null
import { Button } from 'vant';
export default {
    name: "", 
    data () {
        return {
            type: 1
        }
    },
    methods:{
        initCesium(){
            Cesium.Camera.DEFAULT_VIEW_RECTANGLE = Cesium.Rectangle.fromDegrees(
                75.0, // 东
                0.0, // 南
                140.0, // 西
                60.0 // 北
            )
            viewer = new Cesium.Viewer("cesiumContainer",
                {
                    animation: false,
                    timeline: false,
                    fullscreenButton: false,
                    geocoder: false,
                    baseLayerPicker: false,
                    homeButton: false,
                    sceneModePicker: false,
                    navigationHelpButton: false,
                    selectionIndicator: false,
                    shouldAnimate : true,
                    infoBox: false,
                    // imageryProvider: imageryProvider,
                    baseLayerPicker: false,
                    orderIndependentTranslucency: false,
                    contextOptions: {
                        webgl: {
                            alpha: true,
                        }
                    },
                    sceneMode: 2
                }
            )
            viewer._cesiumWidget._creditContainer.style.display = "none"  //	去除版权信息
            viewer.imageryLayers.get(0).show = false;//不显示底图
            viewer.scene.globe.baseColor = Cesium.Color.fromCssColorString("rgba(65, 175, 227, 1)")
            viewer.scene.globe.depthTestAgainstTerrain = false
            viewer.camera.setView({
                // Cesium的坐标是以地心为原点，一向指向南美洲，一向指向亚洲，一向指向北极州
                // fromDegrees()方法，将经纬度和高程转换为世界坐标
                destination:Cesium.Cartesian3.fromDegrees(92.64, 33.93, 3000000.0),
                orientation:{
                    // 指向
                    heading:Cesium.Math.toRadians(180,0),
                    // 视角
                    pitch:Cesium.Math.toRadians(-90),
                    roll:0.0
                }
            })
        },
        getPosition(){
            this.axios.get(`http://xxx.xxx.xxx.xxx:xxxx/getMap.json`, {
            }).then((res) => {
                console.log(res)
                nodesPosition = res.data
                this.createNodes()
                this.createLines()
                this.addPrimitives()
            }).catch((err) => {
                console.log(err)
            })
        },
        createNodes(){
            nodesPosition.bus.forEach( point => {
                nodesInstances.push( new Cesium.GeometryInstance( {
                    geometry : new Cesium.CircleGeometry( {
                        center : Cesium.Cartesian3.fromDegrees( point.lng, point.lat, 1000 ),
                        radius : 30000.0,
                        vertexFormat : Cesium.PerInstanceColorAppearance.VERTEX_FORMAT
                    }),
                    id: point.name,
                    attributes : {
                        color : Cesium.ColorGeometryInstanceAttribute.fromColor( Cesium.Color.fromHsl(0.58, 1, 0.5, 0.5))
                    },
                }))
                labels.add({
                    position: new Cesium.Cartesian3.fromDegrees(point.lng, point.lat, 200000),
                    text: point.name.slice(3),
                    font: '20px Helvetica',
                    horizontalOrigin : Cesium.HorizontalOrigin.CENTER,
                    verticalOrigin : Cesium.VerticalOrigin.CENTER,
                })
            })
            nodesPosition.gen.forEach( point => {
                GInstances.push( new Cesium.GeometryInstance( {
                    geometry : new Cesium.CircleGeometry( {
                        center : Cesium.Cartesian3.fromDegrees( point.lng, point.lat ),
                        radius : 20000.0,
                    }),
                }))
            })
            nodesPosition.load.forEach( point => {
                LoadInstances.push( new Cesium.GeometryInstance( {
                    geometry : new Cesium.CircleGeometry( {
                        center : Cesium.Cartesian3.fromDegrees( point.lng, point.lat ),
                        radius : 20000.0,
                    })
                }))
            })
        },
        createLines(){
            let lines = nodesPosition.busLine.concat(nodesPosition.GenLine.concat(nodesPosition.LoadLine))
            lines.forEach( (line) => {
                linesInstances.push( new Cesium.GeometryInstance( {
                    geometry : new Cesium.PolylineGeometry({
                        positions : Cesium.Cartesian3.fromDegreesArray([
                            line.Flng, line.Flat,
                            line.Tlng, line.Tlat
                        ]),
                        width: 1
                    }),
                    id: line.Line_name,
                    attributes : {
                        color : Cesium.ColorGeometryInstanceAttribute.fromColor( Cesium.Color.WHITE)
                    },
                }))
            })
        },
        addPrimitives(){
            viewer.scene.primitives.add( new Cesium.Primitive( {
                geometryInstances : nodesInstances, //合并
                //某些外观允许每个几何图形实例分别指定某个属性，例如：
                appearance : new Cesium.PerInstanceColorAppearance()
            }))
            viewer.scene.primitives.add( new Cesium.Primitive( {
                geometryInstances : linesInstances, 
                appearance : new Cesium.PolylineColorAppearance({
                    translucent: false,  //是否透明
                })
            }))
            viewer.scene.primitives.add( new Cesium.Primitive( {
                geometryInstances : GInstances, 
                appearance :new Cesium.MaterialAppearance({
                    material: new Cesium.Material({
                        fabric: {
                            type: "Image",
                            uniforms: {
                                image: require('@/assets/images/G.png')
                            }
                        }
                    }),
                    materialSupport : Cesium.MaterialAppearance.MaterialSupport.TEXTURED
                })
            }))
            viewer.scene.primitives.add( new Cesium.Primitive( {
                geometryInstances : LoadInstances,
                appearance :new Cesium.MaterialAppearance({
                    material: new Cesium.Material({
                        fabric: {
                            type: "Image",
                            uniforms: {
                                image: require('@/assets/images/load.png')
                            }
                        }
                    }),
                    materialSupport : Cesium.MaterialAppearance.MaterialSupport.TEXTURED
                })
            }))
        },
        updateNodes(){
            let p = viewer.scene.primitives.get(1)
            console.log(p)
            // 连接 websocket
            websocketMap = new WebSocket('ws://xxx.xxx.xxx.xx:xxxx/Transient')
            websocketMap.onopen = function (event){
                console.log("websocketMap建立链接")
                websocketMap.send('1')
            }
            websocketMap.onmessage = function(event){
                let data = JSON.parse(event.data)
                // console.log(data)
                for(let i = 0;i < data.bus.length; i++){
                    let attributes = p.getGeometryInstanceAttributes(`bus${data.bus[i].id}`)
                    attributes.color = Cesium.ColorGeometryInstanceAttribute.toValue(Cesium.Color.fromHsl(data.bus[i].value*0.25, 1, 0.5, 0.8))
                }
            }
        },
        changeType(type, event){
            console.log('发送数据', type, event.target)
            let b = event.target
            this.type = type
            websocketMap.send(type)
        }
    },
    created(){
        
    },
    mounted() {
        this.initCesium()
        labels = viewer.scene.primitives.add(new Cesium.LabelCollection())
        this.getPosition()
        setTimeout(() => {
            this.updateNodes()
        }, 1000)
        
    }
}
</script> 
<style scoped>
.main{
    width: 100%;
    height: 100%;
}
#cesiumContainer{
    width: 100%;
    height: 100%;
}
.btn{
    width: 400px;
    height: 80px;
    position: absolute;
    top: 100px;
    left: 50%;
    margin-left: -200px
}
</style>

```