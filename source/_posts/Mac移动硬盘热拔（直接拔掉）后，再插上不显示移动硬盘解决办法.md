---
title: Mac移动硬盘热拔（直接拔掉）后，再插上不显示移动硬盘解决办法
date: 2021-11-24 10:13:37
tags: [Mac]
---
1. 打开终端输入：
    ```bash
    diskutil list
    ```
    可以看到，输出结果的最后一行硬盘的名称，因此/dev/disk2所在目录就是移动硬盘
2. 使用挂载命令，输入密码，将硬盘重新挂载
    ```bash
    sudo  fsck_hfs -fy /dev/disk2
    sudo diskutil mount /dev/disk2
    ```

养成良好的习惯，不使用移动硬盘的时候手动「推出」。