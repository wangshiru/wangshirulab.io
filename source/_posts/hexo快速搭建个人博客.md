---
title: 使用hexo快速建立属于自己的blog
date: 2019-01-14 09:30:53
tags: [Hexo]
---
使用HEXO快速搭建个性化BLOG

# 环境
1. 使用命令行工具 [Git](https://git-scm.com/download/linux) 管理和部署代码
2. 安装 [Node](https://nodejs.org/en/) 并安装 [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

# 步骤
1. 注册 [github](https://github.com/) 账号并配置 ssh
2. 在github上新建一个项目,注意项目名为 用户名.github.io
3. 使用npm快速安装 [hexo](https://hexo.io/)
```
hexo init  //初始化hexo
cd hexo //进入项目文件
npm install //安装依赖包
```
4. 安装喜欢的 [主题](https://hexo.io/themes/)
```
git clone 主题的下载地址.git
npm install 主题插件
```
5. 主题安装完成后对 hexo 的根目录下的 _config.yml 进行修改
```
title:  博客标题
subtitle: 副标题
description: 网站描述
language: 语言
url: 站点网址(必填,否则会报错)

deploy:
  type: git
  repo: ssh网址.git
  branch: master
<!--其他视情况填写即可-->
```
6. 对主题进行个性化配置,修改主题目录下的 _config.yml
7. 生成网页 , 本地预览
```
hexo generate
hexo server
```
8. 新建 blog
```
hexo new "文章标题"
```
    *在 source/_posts 目录下会生成文章标题.md*
```
title: 文章标题
date: 2015-07-30 07:56:29 #发表日期，一般不改动
categories: 文章分类
tags: [github,hexo] 文章标签
---
正文，使用Markdown语法书写
```
9. 部署到 github 的项目上
```
hexo deploy
```
    *部署成功会提示*
```
[info] Deploy done: git
```
10. 访问 https://github用户名.github.io 查看自己的博客

11. 部署到 [gitlab pages](https://hexo.io/zh-cn/docs/gitlab-pages)

# 常见问题
1. 提示 Cannot read property ‘replace’ of null 错误 ——–> _config.yml 中 url 未填写导致
2. 代码部署未更新 ——–> hexo clean 后执行 hexo deploy 即可
3. 每次部署都需要输入 github 用户名和密码 ———> 检查 _config.yml 中配置的链接是 ssh 还是 https 链接
4. 自定义的样式无效 ——-> _config.yml 中设置 cdn: false
5. 本地仓库与远程仓库绑定
```bash
git remote rm origin
git remote add origin ***
```