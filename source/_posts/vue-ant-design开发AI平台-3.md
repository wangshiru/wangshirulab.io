---
title: vue+ant-design开发AI平台(3)
date: 2020-05-22 11:01:40
tags: [vue,ant-design]
---

# 部署的一些问题
## 打包命令

```
yarn run bulid
```

生成dist文件夹，将文件夹中的内容放到服务器的tomcat或nginx的端口下。

## 使用mock数据
项目的登录功能调用部分mock接口，想部署之后继续使用，所以需要修改mock文件夹下的index.js

```js
process.env.VUE_APP_PREVIEW === 'false'
```

## 用户注销后找不到登录页面
修改 UserMenu.vue

```js
handleLogout () {
    this.$confirm({
    title: '提示',
    content: '真的要注销登录吗 ?',
    onOk: () => {
        return this.Logout({}).then(() => {
        setTimeout(() => {
            // 控制页面跳转到login界面
            this.$router.push({ name: 'login' })
        }, 16)
        }).catch(err => {
        this.$message.error({
            title: '错误',
            description: err.message
        })
        })
    },
    onCancel () {
    }
    })
}
```

## 配置目录
在 vue.config.js 中添加 publicPath

```js
const vueConfig = {
  publicPath: './',
}
```