---
title: cesium离线部署
date: 2020-05-14 12:32:33
tags: [Cesium.js]
top: 2
---
# 在线地形地图

## 基本使用

在线使用 [Cesium ion](https://cesium.com/ion/assets/1) Cesium World Terrain

```js
// Create Cesium World Terrain with default settings
var viewer = new Cesium.Viewer('cesiumContainer', {
    terrainProvider: Cesium.createWorldTerrain()
});

// Create Cesium World Terrain with water and normals.
var viewer = new Cesium.Viewer('cesiumContainer', {
    terrainProvider : Cesium.createWorldTerrain({
        requestWaterMask : true,
        requestVertexNormals : true
    });
});
```

或

```js
var viewer = new Cesium.Viewer('cesiumContainer',{
    terrainProvider : new Cesium.CesiumTerrainProvider({
        url: Cesium.IonResource.fromAssetId(1)
    })
});
```

## Cesium服务key

```js
Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI1NWI5MGUzNi1mYWI3LTQzY2QtOGI0Ni0xZWYyNTAxNGM4N2MiLCJpZCI6MTI1OTgsInNjb3BlcyI6WyJhc3IiLCJnYyJdLCJpYXQiOjE1NjE0NDkyNTV9.hBH0PGSnKErc_yNhIePASUkr3QPDoo0KDX9uLpNBUns';
```

## 定义viewer隐藏工具

```js
var viewer = new Cesium.Viewer('cesiumContainer',{
    animation: false,
    timeline: false,
    fullscreenButton: false,
    geocoder: false,
    baseLayerPicker: false,
    homeButton: false,
    sceneModePicker: false,
    navigationHelpButton: false,
    selectionIndicator: false,
    shouldAnimate : true,
    infoBox: false,
    terrainProvider : new Cesium.CesiumTerrainProvider({
        url: Cesium.IonResource.fromAssetId(1)
    })
});
```

## 隐藏版权信息

```js
viewer._cesiumWidget._creditContainer.style.display = "none"; 
```

瓦片是使用DEM贴图，不是立体模型。两种数据：瓦片、DEM高程数据。

# 离线数据准备工作
使用水经注软件分别下载影像数据和高程数据

## 影像数据下载
* 选择卫星，谷歌全球无偏移；
* 点击下载，框选范围，可以多选，会自动剪裁；
* 双击弹出下载框，选择要下载的级别，然后进行导出设置；
* 选择导出瓦片，标准TMS瓦片，png格式，WGS84经纬度投影，背景颜色透明；
* 点击确定开始下载。

## 高程数据下载
* 进入软件后选择高程，区划选择中国，回车；
* 点击下载，导出设置，选择导出瓦片，高程CES，点击下载。

# 文件部署
部署在自己的服务器上，可以使用tomcat等发布，发布之后能根据访问文件夹即可。

# cesium如何引入

在1.69版本中，使用TileMapServiceImageryProvider引入影像数据。

## 影像数据

```js
var viewer = new Cesium.Viewer('cesiumContainer',{ 
    imageryProvider:new Cesium.TileMapServiceImageryProvider({
              url: 'http://127.0.0.1:5500/'
          }),
    baseLayerPicker:false 
}); 
```

## 高程数据

```js
var terrainProvider = new Cesium.CesiumTerrainProvider({
    url: 'http://127.0.0.1:5501/'
});
viewer.terrainProvider = terrainProvider;
```
