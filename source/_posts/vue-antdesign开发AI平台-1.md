---
title: vue+ant-design开发AI平台(1)
date: 2020-05-19 19:11:53
tags: [vue,ant-design]
---
# 项目准备
[ant design pro vue官网](https://pro.loacg.com/) [组件参考文档](https://antdv.com/docs/vue/introduce-cn/)

```bash
$ git clone --depth=1 https://github.com/sendya/ant-design-pro-vue.git my-project
$ cd my-project
$ yarn install
$ yarn run serve
```

# 登录功能

## 表单添加 :form="form" 方便获取表单各个字段，每个字段都放在 a-form-item 中。

```jsx
<a-form
    id="formLogin"
    class="user-layout-login"
    ref="formLogin"
    :form="form"
    @submit="handleSubmit"
    layout="vertical"
>
    <a-form-item label="用户名">
        <a-input
            size="small"
            type="text"
            placeholder="账户: admin"
            v-decorator="[
            'username',
            {rules: [{ required: true, message: '请输入用户名' }, { validator: handleUsernameOrEmail }], validateTrigger: 'change'}
            ]"
        >
            <a-icon slot="prefix" type="user" :style="{ color: 'rgba(0,0,0,.25)' }"/>
        </a-input>
    </a-form-item>

    <a-form-item label="密码">
        <a-input
            size="small"
            type="password"
            autocomplete="false"
            placeholder="密码: admin"
            v-decorator="[
            'password',
            {rules: [{ required: true, message: '请输入密码' }], validateTrigger: 'blur'}
            ]"
        >
            <a-icon slot="prefix" type="lock" :style="{ color: 'rgba(0,0,0,.25)' }"/>
        </a-input>
    </a-form-item>
    <a-form-item>
        <a-button
            size="small"
            type="primary"
            htmlType="submit"
            class="login-button"
            :loading="state.loginBtn"
            :disabled="state.loginBtn"
        >登陆</a-button>
    </a-form-item>
</a-form>
```
## 以下是 :form="form" 中的form

```js
data () {
    return {
        form: this.$form.createForm(this)
    }
  }
```

## methods 中获取表单的字段并传给后端验证

```js
handleSubmit (e) {
    validateFields(validateFieldsKey, { force: true }, (err, values) => {
    if (!err) {
        <!-- values是校验后的表单各个字段 -->
        Login(values)
        .then((res) => this.loginSuccess(res))
        .catch(err => this.requestFailed(err))
        .finally(() => {
            state.loginBtn = false
        })
    } else {
        setTimeout(() => {
        state.loginBtn = false
        }, 600)
    }
    })
},
```

## src/api/login.js 定义后端api登录校验接口

```js
export function login (parameter) {
  return axios({
    url: '接口地址',
    method: 'post',
    data: parameter
  })
}
```

## 调用登录api，更改 Vuex 的 store 中的状态的唯一方法是提交 mutation。

```js
    const user = {
    state: {
        token: '',
        name: '',
        welcome: '',
        avatar: '',
        roles: [],
        info: {}
    },
    mutations: {
        SET_TOKEN: (state, token) => {
        state.token = token
        },
        SET_NAME: (state, { name, welcome }) => {
        state.name = name
        state.welcome = welcome
        },
        SET_AVATAR: (state, avatar) => {
        state.avatar = avatar
        },
        SET_ROLES: (state, roles) => {
        state.roles = roles
        },
        SET_INFO: (state, info) => {
        state.info = info
        }
    },
    actions: {
    Login ({ commit }, userInfo) {
        return new Promise((resolve, reject) => {
        login(userInfo).then(response => {
            if (response.success) {
            Vue.ls.set(ACCESS_TOKEN, '4291d7da9005377ec9aec4a71ea837f', 7 * 24 * 60 * 60 * 1000)
            <!-- 需要校验码，没有校验码进不去系统 -->
            
            commit('SET_TOKEN', '4291d7da9005377ec9aec4a71ea837f')

            <!-- 
                把用户名保存到state中,使用如下方式调用
                computed: {
                    ...mapState({
                    name: (state) => state.user.name
                    })
                } 
            -->

            commit('SET_NAME', { name: response.detail.username, welcome: welcome() })
            resolve('登录成功')
            } else {
            resolve('用户名或密码错误')
            }
        }).catch(error => {
            reject(error)
        })
        })
    }
    }
```

## 后端api返回验证结果后跳转到系统主页，欢迎弹窗。

```js
loginSuccess (res) {
    if (res === '登录成功') {
    this.$router.push({ name: 'Analysis' }, () => {
        console.log('onComplete')
        this.$notification.success({
        message: '欢迎',
        description: `${timeFix()}，欢迎回来`
        })
    })
    }
}
```