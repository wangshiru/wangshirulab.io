---
title: showDoc私有部署
date: 2021-04-15 10:45:11
tags: [工具]
---

# 安装

[官方安装文档](https://www.showdoc.com.cn/help?page_id=65610)

安装前请确保你的环境已经装好了docker.

```bash
# 中国大陆镜像安装命令（安装后记得执行docker tag命令以进行重命名）
docker pull registry.cn-shenzhen.aliyuncs.com/star7th/showdoc
docker tag registry.cn-shenzhen.aliyuncs.com/star7th/showdoc:latest star7th/showdoc:latest 
# 新建存放showdoc数据的目录
mkdir -p /showdoc_data/html
# 修改读写权限
chmod  -R 777 /showdoc_data
# 启动showdoc容器，注意该端口需要在云服务器供应商处放行
docker run -d --name showdoc --user=root --privileged=true -p 13500:80 -v /showdoc_data/html:/var/www/html/ star7th/showdoc
# 查看是否已经跑起来了
docker ps | grep showdoc
# 打开防火墙
firewall-cmd --add-port=13500/tcp --permanent
```