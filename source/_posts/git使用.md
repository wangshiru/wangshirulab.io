---
title: git使用
date: 2020-05-25 11:22:48
tags: [git]
---
# 切换分支

```bash
# 创建分支
git branch mybranch
# 切换分支
git checkout mybranch
# 创建并切换分支
git checkout -b mybranch
```

# 文件比较

```
git diff
```

# 取消所有变更

```bash
git checkout .
```

# 代码合并

子分支
```bash
git checkout 子分支
git add .
git commit -m '备注'
git push origin 子分支
```

管理员处理合并
```bash
# 先切换到子分支，拉代码
git checkout 子分支
git pull
# 代码无误后切换到主分支，合并子分支的代码
git checkout 主分支
git merge 子分支名
# 没有冲突时
git push origin 主分支
# 有冲突时，解决冲突，再提交
git add .
git commit -m '备注'
git push origin 主分支
# 将本地dev推送到远端dev
git push origin dev:dev
# 查看以往操作
git reflog
# 切换到某个操作
git reset --hard hash值
```

# 将变更缓存

```bash
# 将变更缓存
git stash
git checkout 另一个分支
# 释放更改
git stach pop
```

# 提交时提示Your branch is ahead of 'origin/master' by 1 commit

```bash
git branch --set-upstream-to=origin/dev
```
然后正常 git push 即可

[参考](https://www.cnblogs.com/woailiming/p/11444291.html)

# 打标签

## 查看标签

```bash
git tag
```

## 创建标签

```bash
git tag -a v1.0 -m "备注信息"
```

## 对过去的提交打标签

```bash
git log --pretty=oneline
9fceb02d0ae598e95dc970b74767f19372d61af8 updated rakefile
964f16d36dfccde844893cac5b347e7b3d44abbc commit the todo
8a5cbc430f1a9c3d00faaeffd07798508422908a updated readme
git tag -a v1.2 9fceb02
```

## 共享标签

默认情况下，git push 命令并不会传送标签到远程仓库服务器上。 在创建完标签后你必须显式地推送标签到共享服务器上。 这个过程就像共享远程分支一样——你可以运行 git push origin [tagname]

```bash
git push origin v1.5
Counting objects: 14, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (12/12), done.
Writing objects: 100% (14/14), 2.05 KiB | 0 bytes/s, done.
Total 14 (delta 3), reused 0 (delta 0)
To git@github.com:schacon/simplegit.git
 * [new tag]         v1.5 -> v1.5
```

如果想要一次性推送很多标签，也可以使用带有 --tags 选项的 git push 命令。 这将会把所有不在远程仓库服务器上的标签全部传送到那里。

```bash
git push origin --tags
Counting objects: 1, done.
Writing objects: 100% (1/1), 160 bytes | 0 bytes/s, done.
Total 1 (delta 0), reused 0 (delta 0)
To git@github.com:schacon/simplegit.git
 * [new tag]         v1.4 -> v1.4
 * [new tag]         v1.4-lw -> v1.4-lw
```

## 删除标签

要删除掉你本地仓库上的标签，可以使用命令 

```bash
git tag -d <tagname>
```

应该注意的是上述命令并不会从任何远程仓库中移除这个标签，你必须使用 git push <remote> :refs/tags/<tagname> 来更新你的远程仓库：

```bash
$ git push origin :refs/tags/v1.4-lw
To /git@github.com:schacon/simplegit.git
 - [deleted]         v1.4-lw
```

[参考](https://git-scm.com/book/zh/v2/Git-%E5%9F%BA%E7%A1%80-%E6%89%93%E6%A0%87%E7%AD%BE)

# Git flow 

## 模型1

适用于持续集成多环境场景，相对稳定，持续迭代

bug -> new branch -> master -> pre branch (pre-production) -> target branch (production)

## 模型2

适用于版本项目，稳定版本从master检出，bug修复在分支

master -> stable -> new branch -> bug fix -> version


# 多人协同

## 本地新建 dev 分支

```bash
# 新建分支
git branch dev
# 查看分支
git branch
# 切换到分支
git checkout dev
# 删除分支
git branch dev -D
# 创建并切换分支到 dev
git checkout -b dev
# B 修改文件 test.txt 并推送代码到远端 dev 分支
# A 修改文件 test.txt 并推送代码到远端 dev 分支，会报冲突错误，A 需要
git pull origin dev
# A 在本地解决冲突，删除不需要的代码，再次推送代码到远端 dev 分支，完成一次协作
# B 先 git fetch 拉取远程分支，但不会合并，会将远程更新拉取到 FETCH_HEAD 分支
# B 可以选择性合并 FETCH_HEAD 分支，更新冲突文件的代码
git merge FETCH_HEAD
```