---
title: redis基础
date: 2021-05-07 10:11:35
tags: [数据库]
---

# 特点
* 高性能，可持久化
* key-value结构，支持多种数据类型
* 支持事务，数据原子性（要么不做/要么全做）

# 应用场景

* 缓存（读写性能优异）
* 计数&消息系统（高并发、发布/订阅阻塞队列功能）
* 分布式会话session & 分布式锁（秒杀）

# redis vs mongo

* 存储方式不一样：key-value vs document
* 使用方式&可靠性不一样：MongoDB SQL & ACID支持
* 应用场景不一样：高性能缓存 vs 海量数据分析

# 安装

[docker 安装 redis](https://hub.docker.com/_/redis/)

## docker-compose

*docker-compose.yml*

```yml
version: "3",
services:
    redis-test:
        image: "redis",
        restart: always,
        container_name: "redis-test",
        ports:
            - 15001:6379
        volumes:
            - /home/redistest:/data
        command: ["redis-server", "--requirepass", "123456"]
```

```bash
docker-compose up -d
```

## docker run 命令

```bash
docker run -itd --restart=always -p 15001:6379 -v /home/redistest:/data redis redis-server --requirepass 123456
```

# redis cli

[redis命令参考](http://doc.redisfans.com/)

```bash
# 进入容器
docker exec -it redis-test /bin/bash
# 进入redis
redis-cli
# 鉴权
auth 123456
# 参考文档进行操作
```