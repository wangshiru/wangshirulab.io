---
title: mac homebrew安装报错
date: 2020-10-27 15:23:33
tags: [mac]
---

# 问题描述

mac 安装homebrew出错 Failed to connect to raw.githubusercontent.com port 443: Connection refused error:

# 解决方式

1. 修改hosts

sudo vim /etc/hosts

2. 添加如下内容：

199.232.28.133 raw.githubusercontent.com

[参考]](https://blog.csdn.net/txl910514/article/details/105880125)

# brew安装

1. 确认安装ruby

```
ruby --version
```

2. 安装(速度比较慢)

```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

3. 确认

```
brew --version
```

[参考](https://www.jianshu.com/p/73fd28be827f)