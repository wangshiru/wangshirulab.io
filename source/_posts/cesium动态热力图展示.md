---
title: cesium动态热力图展示
date: 2020-05-14 12:32:49
tags: [Cesium.js]
top: 2
---

# 插件引入
* heatmap.min.js
* HeatmapImageryProvider.js

# 代码

```js
// 测试数据
var heatmapdata = [
    {
        x: '123.307279',
        y: '46.375662',
        value: 80
    },
    {
        x: '115.433236',
        y: '41.707946',
        value: 100
    },
    {
        x: '96.888761',
        y: '39.120975',
        value: 90
    }
]

// 创建图层集合
var imageryLayers = viewer.imageryLayers;
// 声明热力图层
var heatmapImagery
function create_heatmap(){
    // 创建图层并赋值给heatmapImagery
    heatmapImagery = new Cesium.ImageryLayer(createHeatmapImageryProvider(Cesium, {
        data: {
            min: 0,
            max: 100,
            points: heatmapdata
        }
    }))
}
create_heatmap()

// 添加热力图层
viewer.imageryLayers.add(heatmapImagery)

// 删除热力图层
viewer.imageryLayers.remove(heatmapImagery)
```

想动态展示热力图，数据需要重新渲染，在生成新的图层之前，删除原来的图层，再将新的图层加到集合中。
*注意：viewer.imageryLayers.remove(),删除的是layer，不是删除ImageryProvide*