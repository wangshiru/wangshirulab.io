---
title: node启动本地http和https服务
date: 2020-10-27 15:30:07
tags: [node]
---

# http

server.js

```js
'use strict'

var http = require('http');

var app = http.createServer(function(req,res){

	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.end('hello world');

}).listen(9090, '0.0.0.0');

```

安装 forever ，使服务端口在后台运行，不会因为终端被杀死而停止。
使用 `forever star server.js` 启动端口
使用 `forever stop server.js` 停止端口

# https

```js
'use strict'

var https = require('https');
var fs = require('fs');

var options = {
	key: fs.readFileSync(''),
	cert: fs.readFileSync(''),
}

var app = https.createServer(options, function(req, res){
	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.end('hello world');
}).listen(443, '0.0.0.0');
```

使用 https 需要本地创建证书

[参考](https://blog.csdn.net/weixin_43377336/article/details/88060775)
