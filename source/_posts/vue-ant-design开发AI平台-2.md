---
title: vue+ant-design开发AI平台(2)
date: 2020-05-20 09:50:02
tags: [vue,ant-design]
---
# 样式修改
[官方样式修改文档](https://pro.loacg.com/docs/style)

```jsx
<style lang="less" scoped>
// 使用 css 时可以用 >>> 进行样式穿透
.test-wrapper >>> .ant-select {
    font-size: 16px;
}

// 使用 scss, less 时，可以用 /deep/ 进行样式穿透
.test-wrapper /deep/ .ant-select {
    font-size: 16px;
}
</style>
```

# 表格的展示
## 表格每列信息根据columns展示，数据源为data-source，每行的id作为rowKey，因为要更改默认样式，给table加class，方便样式穿透。

```jsx
<a-table :columns="columns" :data-source="data" rowKey="id" class="sample-table">
    <span slot="action" slot-scope="text, record">
    <!-- 
      开关初始状态default-checked为true或false，根据后端的数据字段判断
      点击出发修改行信息字段，把行id传回后端
      修改后，页面重新渲染表格内信息，是否开启状态已改变
      change先于click，所以用change先传id，再用click调用后端api
     -->
    <a-switch :default-checked="record.switch" @change="() => change(record.id)" @click="onClick" size="small">
        <a-icon slot="checkedChildren" type="check" />
        <a-icon slot="unCheckedChildren" type="close" />
    </a-switch>
    </span>
</a-table>
```

## columns中每个对象为一列的信息，title为表头名称，dataIndex为列数据在数据项中对应的 key
使用 columns 时，可以通过slots配置支持 slot 的属性，如 slots: { filterIcon: 'XXX'}
使用 columns 时，可以通过scopedSlots配置支持 slot-scope 的属性，如 scopedSlots: { customRender: 'XXX'}

```js
<script>
const columns = [
  {
    title: '序号',
    dataIndex: 'no',
    key: 'no',
    slots: { title: 'no' },
    scopedSlots: { customRender: 'no' }
  },
  {
    title: '故障集',
    dataIndex: 'fault_set',
    key: 'fault_set'
  },
  {
    title: '输出格式描述',
    dataIndex: 'output_format_des',
    key: 'output_format_des'
  },
  {
    title: '状态',
    key: 'is_share',
    dataIndex: 'is_share',
    scopedSlots: { customRender: 'is_share' }
  },
  {
    title: '用户',
    key: 'username',
    dataIndex: 'username',
    scopedSlots: { customRender: 'username' }
  },
  {
    title: '是否共享',
    key: 'action',
    scopedSlots: { customRender: 'action' }
  }
]
</script>
```

使用vuex获取值

```js
export default {
  data () {
    return {
      data,
      columns,
      changeId,
      form: this.$form.createForm(this)
    }
  },
  computed: {
    ...mapState({
      <!-- 使用vuex获取username -->
      name: (state) => state.user.name
    })
  }
}
```

# 模态框

```vue
<template>
  <a-button type="primary" @click="showModal" class="upload-btn">
    本地上传
  </a-button>

  <!-- 使用visible控制模态框的显示隐藏 -->
  <a-modal
    title="样本数据导入"
    :visible="visible"
    :confirm-loading="confirmLoading"
  >
    <template slot="footer">
      <a-button key="back" @click="handleCancel">
        取消
      </a-button>
      <a-button
        type="primary"
        :disabled="fileList.length === 0"
        :loading="uploading"
        style="margin-top: 16px"
        @click="handleUpload"
      >
      <!-- 使用手动上传，点击提交后调用后端api使用axios传输表单 -->
        {{ uploading ? "上传中" : "开始上传" }}
      </a-button>
    </template>
    <a-form layout="horizontal" :form="form">
      <a-form-item
        label="描述"
        :label-col="{ span: 4 }"
        :wrapperCol="{ span: 12 }"
      >
        <a-input
          v-decorator="[
            'describe',
            { rules: [{ required: true, message: '请输入文字描述' }] },
          ]"
          placeholder="请输入文字描述" />
      </a-form-item>
      <a-form-item
        label="故障集"
        :label-col="{ span: 4 }"
        :wrapperCol="{ span: 12 }"
      >
        <a-select
          mode="multiple"
          style="width: 100%"
          placeholder="请选择故障集"
          @change="handleChangeSelect"
          v-decorator="[
            'faultSet',
            { rules: [{ required: true, message: '请选择故障集', type: 'array' }] },
          ]"
        >
          <!--
            故障集为后端接口返回的数组，需要遍历
            key为遍历的每个对象唯一标识，value为值，当未定义value时，选中的返回数组为key，否则为value
          -->
          <a-select-option v-for="( item ) in this.faultSet" :key="item.no" :value="item.fault_name">
            {{ item.fault_name }}
          </a-select-option>
        </a-select>
      </a-form-item>
      <a-form-item
        label="上传"
        :label-col="{ span: 4 }"
        :wrapperCol="{ span: 12 }"
      >
        <!-- 使用手动上传需要设置before-upload -->
        <a-upload
          v-decorator="[
            'file',
            {
              valuePropName: 'file',
              rules: [{ required: true, message: '请选择文件' }]
            },
          ]"
          name="file"
          :file-list="fileList"
          :remove="handleRemove"
          :before-upload="beforeUpload"
        >
          <a-button> <a-icon type="upload" /> 选择文件 </a-button>
        </a-upload>
      </a-form-item>
      <a-form-item
        label="是否启动"
        :label-col="{ span: 4 }"
        :wrapperCol="{ span: 12 }"
      >
        <a-checkbox
          @change="onChange"
          v-decorator="[
            'start',
            { rules: [{ required: false, message: '请选择' }] },
          ]"
          name="start">
          是否启动样本数据的制造
        </a-checkbox>
      </a-form-item>
    </a-form>
  </a-modal>
</template>
```

使用的方法如下：

```js
methods: {
  handleChangeSelect (value) {
    console.log(`selected ${value}`)
  },
  beforeUpload (file) {
    this.fileList = [...this.fileList, file]
    return false
  },
  showModal () {
    this.visible = true
  },
  handleUpload () {
    const { fileList } = this
    const formData = new FormData()
    <!-- 
      使用formData传值，作用如下：
      1.将form表单元素的name与value进行组合，实现表单数据的序列化，从而减少表单元素的拼接，提高工作效率。
      2.异步上传文件
    -->
    this.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values)
        formData.append('username', this.name)
        formData.append('describe', values.describe)
        formData.append('faultSet', values.faultSet)
        formData.append('start', values.start)
      }
    })
    fileList.forEach(file => {
      formData.append('file', file)
    })
    this.uploading = true
    <!-- axios返回对象是promise -->
    axios({
      method: 'post',
      url: '后端api地址',
      data: formData,
      headers: {
          'Content-Type': 'multipart/form-data'
      }
    }).then((response) => {
        if (response.data.success) {
          this.fileList = []
          this.uploading = false
          this.visible = false
          <!-- 重置表单，清空所有属性 -->
          this.form.resetFields()
          console.log('上传成功')
          manageShowSample({ 'username': this.name }).then((res) => {
            this.data = []
            if (res.msg === '访问成功') {
              this.data = []
              for (var val of res.detail) {
                if (val.is_share) {
                  val.switch = true
                  val.is_share = '已共享'
                } else {
                  val.switch = false
                  val.is_share = '未共享'
                }
                this.data.push(val)
              }
              console.log(this.data)
            } else {
            }
          })
        } else {
          this.form.resetFields()
          this.uploading = false
          this.visible = false
          console.log('上传失败' + response.data.msg)
        }
    })
  }
}
```