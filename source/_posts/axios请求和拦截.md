---
title: axios请求和拦截
date: 2020-08-05 16:52:58
tags: [JavaScript]
---
[参考](https://www.itsource.cn/web/news/2092.html)

## 发送请求
1. get

```js
axios.get('http://xxx.xxx.xxx')
    .then(function (response) {
        console.log(response)
    })
    .catch(function (error) {
        console.log(error)
    })
```

2. post

```js
axios.get('http://xxx.xxx.xxx', 
    {
        username: xxx
    }
    ).then((res) => {
        console.log(res)
    }).catch((err) => {
        console.log(err)
    })
```

## 拦截器

在项目中，开发者经常会遇到两种情景：
一：在每次发送请求时，携带用户token方便后台做用户相关操作
二：服务器所有接口都会返回一个错误码，比如10001，请求参数错误。 10002，用户未授权等等，我们需要在收到响应后对错误码进行相关提示。
如果我们在每个接口都进行验证那太麻烦，代码冗余不说，后期维护成本也很高，到处找接口进行代码改动。Axios提供了非常方便的一种功能：请求/响应拦截器。我们就可以在这种场景派上用场。

1. 请求拦截器(在每次发送请求时，携带用户token方便后台做用户相关操作)

```js
axios.interceptors.request.use(
    config => {
        //config是axios配置对象
        //每次发送请求前都会进入此拦截器处理函数，可以在此处统一携带上token，每次请求都会自
        config.headers.common['token'] = localStorage.getItem(“token”);
        return config
    },
    err => {
        //请求出错的处理函数
        return Promise.reject(err)
    })
```

2. 响应拦截器(服务器所有接口都会返回一个错误码，比如10001，请求参数错误。 10002，用户未授权等等，我们需要在收到响应后对错误码进行相关提示。)

```js
axios.interceptors.response.use(function(res){
    //收到响应后，可以先统一处理错误码
    switch (res.data.code) {
        case 10001:   alert(‘非法操作’)
    router.push({path: '/})
        case 10002:  alert(‘权限不足’)
        ......
        }
        return res
    },function(err){
        //响应出错进入的函数
        return Promise.reject(err)
    });
}
```