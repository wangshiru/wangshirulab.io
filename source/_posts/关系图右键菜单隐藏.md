---
title: 关系图右键菜单隐藏
date: 2020-05-30 17:10:05
tags: [关系图]
---
# 隐藏浏览器默认右键菜单

添加某类型节点时，需要再相应图标上右键弹出菜单，需要关闭浏览器默认菜单，展示自定义菜单

```javascript
window.onclick=function(e){
  //用户触发click事件就可以关闭了，因为绑定在window上，按事件冒泡处理，不会影响菜单的功能
  document.querySelector('#menu').style.height=0;
}
```

点击图标时，打开自定义菜单

```javascript
$(function() {
  $(".icons").bind("mousedown",function(e){
    var key = e.which; //获取鼠标键位
    if(key == 3)  //(1:代表左键； 2:代表中键； 3:代表右键)
    {
      <!-- 获取右键点击坐标 -->
      var x = e.clientX;
      var y = e.clientY;
      $(".rm").show().css({"position":"absolute","left":x,"top":y});
      var name = $($(this).context.innerHTML).attr("src").split("/")[2].split(".")[0];
      var nodeType = {name:name};
      $("#addbtn").unbind("mousedown").bind("mousedown",nodeType,function(e){
        addnode(e.data.name);
      })
    } 
  })
})
```