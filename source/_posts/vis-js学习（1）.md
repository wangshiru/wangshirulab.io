---
title: vis.js学习（1）
date: 2019-03-30 09:30:53
tags: [关系图]
---
# 介绍
[vis.js](https://visjs.org/) 是基于浏览器的动态可视化库。该库旨在易于使用，处理大量动态数据，以及实现对数据的操作和交互。该库由组件DataSet，Timeline，Network，Graph2d和Graph3d组成。我这里做关系图，所以目前主要针对 Network 进行学习。

# 安装
1. 使用 npm 安装
2. github 直接 clone 或者 下载包

# 目录结构
进入 \vis-master\examples\network 目录
## data目录
### datasets.html
#### 渲染一个基本的 network

```js
function startNetwork() {
    // this list is kept to remove a random node.. we do not add node 1 here because it's used for changes
    nodeIds = [2, 3, 4, 5];
    shadowState = false;
    // create an array with nodes 每个节点有一个唯一的 id ，label 为节点的标签
    nodesArray = [
        {id: 1, label: 'Node 1'},
        {id: 2, label: 'Node 2'},
        {id: 3, label: 'Node 3'},
        {id: 4, label: 'Node 4'},
        {id: 5, label: 'Node 5'}
    ];
    //使用 DataSet 函数将节点和边转化为 vis 节点和边对象
    nodes = new vis.DataSet(nodesArray);
    // create an array with edges 每条边有入度和出度，调用节点 id
    edgesArray = [
        {from: 1, to: 3},
        {from: 1, to: 2},
        {from: 2, to: 4},
        {from: 2, to: 5}
    ];
    edges = new vis.DataSet(edgesArray);
    // create a network 渲染 network 需要 Dom 中有一个 id 为 mynetwork 的 div 作为容器
    var container = document.getElementById('mynetwork');
    //设置渲染数据和参数，用 Network 函数生成即可。可以通过设置 options 来生成自定义 network 
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {};
    network = new vis.Network(container, data, options);
}
```

##### 添加一个随机生成的节点

```js
function addNode() {
	//random() 方法可返回介于 0 ~ 1 之间的一个随机数，1e7表示10000000
	//Number.toString()：将数字转换为字符串。用它的参数指定的基数或底数（底数范围为2-36）。如果省略参数，则使用基数10。当参数值为2时，返回二进制数。，并返回结果
    var newId = (Math.random() * 1e7).toString(32); 
    nodes.add({id:newId, label:"I'm new!"});
}
```

参考博客： [一些有用的JS 代码片段](https://www.jianshu.com/p/0ae00f6b83ac)

#### 改变指定节点的属性并更新

```js
function changeNode1() {
	//Math.floor()返回小于或等于一个给定数字的最大整数
    var newColor = '#' + Math.floor((Math.random() * 255 * 255 * 255)).toString(16);
    //更新指定id的节点属性，传入 id 和更改的节点属性
    nodes.update([{id:1, color:{background:newColor}}]);
}
```

#### 移除指定节点

```js
function removeRandomNode() {
    var randomNodeId = nodeIds[Math.floor(Math.random() * nodeIds.length)];
    //移除指定id节点
    nodes.remove({id:randomNodeId});
    //indexOf() 方法可返回某个指定的字符串值在字符串和数组中首次出现的位置。
    var index = nodeIds.indexOf(randomNodeId);
    //splice() 方法向/从数组中添加/删除项目，然后返回被删除的项目。index规定添加/删除项目的位置，1是要删除的项目数量
    nodeIds.splice(index,1);
}
```

移除节点分两步，先移除指定id节点，然后在节点id数组中移除该id值

#### 修改全局节点属性

```js
function changeOptions() {
    shadowState = !shadowState;
    network.setOptions({nodes:{shadow:shadowState},edges:{shadow:shadowState}});
}
```

#### 重新加载为初始状态

```js
function resetAllNodes() {
    nodes.clear();
    edges.clear();
    nodes.add(nodesArray);
    edges.add(edgesArray);
}
```

#### 重新加载为稳定的初始状态

```js
function resetAllNodesStabilize() {
    resetAllNodes();
    network.stabilize();
}
```

#### 设为稳定状态

```js
function setTheData() {
    nodes = new vis.DataSet(nodesArray);
    edges = new vis.DataSet(edgesArray);
    network.setData({nodes:nodes, edges:edges})
}
```

#### 销毁dom并重新渲染

```js
function resetAll() {
    if (network !== null) {
        network.destroy();
        network = null;
    }
    startNetwork();
}
```

### dynamicData.html

#### 添加节点

```js
function addNode() {
    try {
        nodes.add({
            id: document.getElementById('node-id').value,
            label: document.getElementById('node-label').value
        });
    }
    catch (err) {
        alert(err);
    }
}
```

#### 更新:保存修改后的节点信息（如果不存在id则添加，已存在id则修改）

```js
function updateNode() {
    try {
        nodes.update({
            id: document.getElementById('node-id').value,
            label: document.getElementById('node-label').value
        });
    }
    catch (err) {
        alert(err);
    }
}
```

添加和更新边和节点类似

#### 移除

```js
function removeNode() {
    try {
        nodes.remove({id: document.getElementById('node-id').value});
    }
    catch (err) {
        alert(err);
    }
}
```

#### 渲染

```js
function draw() {
    // create an array with nodes
    nodes = new vis.DataSet();
    nodes.on('*', function () {
    	//JSON.stringify() 方法将 JavaScript 对象转换为字符串
        document.getElementById('nodes').innerHTML = JSON.stringify(nodes.get(), null, 4);
    });
    nodes.add([
        {id: '1', label: 'Node 1'},
        {id: '2', label: 'Node 2'},
        {id: '3', label: 'Node 3'},
        {id: '4', label: 'Node 4'},
        {id: '5', label: 'Node 5'}
    ]);

    // create an array with edges
    edges = new vis.DataSet();
    edges.on('*', function () {
        document.getElementById('edges').innerHTML = JSON.stringify(edges.get(), null, 4);
    });
    edges.add([
        {id: '1', from: '1', to: '2'},
        {id: '2', from: '1', to: '3'},
        {id: '3', from: '2', to: '4'},
        {id: '4', from: '2', to: '5'}
    ]);

    // create a network
    var container = document.getElementById('network');
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {};
    network = new vis.Network(container, data, options);

}
```

参考[JSON.stringify()](https://www.runoob.com/json/json-stringify.html)