---
title: 'html和css基础'
date: 2021-07-22 10:06:19
tags: [html, css]
---
# HTML
## 如何理解 HTML 语义化？
* 让人更容易读懂代码，易读性
* SEO 让搜索引擎更容易读懂

## 默认情况下，哪些 HTML 标签是块级元素、哪些是内联元素？
* display: block/table 有 div h1 h2 table ul ol p
* display: inline/inline-block 有 span img input button

# CSS
## 布局
### 盒子模型的宽度如何计算？

* 题目
```html
<!-- 如下代码，请问 div1 的 offsetWidth 是多大 -->
<style>
#div1{
    width: 100px;
    padding: 10px;
    border: 1px solid #ccc;
    margin 10px;
}
</style>
<div id="div1"></div>
```

* 解答：
offsetWidth = (内容宽度+内边距+边框)。无外边距
答案：122px

* 拓展
如果想让 offsetWidth = 100 ，该怎么做？
=> 添加 box-sizing: border-box

### margin 纵向重叠的问题

* 题目
```html
<!-- 如下代码，AAA 和 BBB 之间的距离是多少？ -->
<style>
p{
    font-size: 16px;
    line-height: 1;
    margin-top: 10px;
    margin-bottom: 15px;
}
</style>
<p>AAA</p>
<p></p>
<p></p>
<p></p>
<p>BBB</p>
```

* 解答：
相邻元素的 margin-top 和 margin-bottom 会发生重叠
空白内容的`<p></p>`也会被重叠
答案：15px

### margin 负值的问题
* 解答：
margin-top 和 margin-left 负值，元素向上、向左移动
margin-right 负值，右侧元素左移，自身不受影响
marhin-bottom 负值，下方元素上移，自身不受影响

### BFC的理解和应用
* Block format context ，块级格式化上下文
* 一块独立渲染区域，内部元素的渲染不会影响边界以外的元素
* 形成条件：
    float 不是 none
    position 是 absolute 或fixed
    overflow 不是 visible
    display 是 flex, inline-block
* 常见应用
    清除浮动

### float 布局的问题，以及 clearfix
#### 如何实现圣杯布局和双飞翼布局
* 目的：
    三栏布局，中间一栏最先加载和渲染（内容最重要）
    两侧内容固定，中间内容随着宽度自适应
* 技术总结：
    使用 float布局
    两侧使用 margin 负值，以便和中间内容横向重叠
    防止中间内容被两侧覆盖，一个用 padding 一个用 margin

* 圣杯布局
```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>圣杯布局</title>
    <style type="text/css">
        body {
            min-width: 550px;
        }
        #header {
            text-align: center;
            background-color: #f1f1f1;
        }

        #container {
            padding-left: 200px;
            padding-right: 150px;
        }
        #container .column {
            float: left;
        }

        #center {
            background-color: #ccc;
            width: 100%;
        }
        #left {
            position: relative;
            background-color: yellow;
            width: 200px;
            margin-left: -100%;
            right: 200px;
        }
        #right {
            background-color: red;
            width: 150px;
            margin-right: -150px;
        }

        #footer {
            text-align: center;
            background-color: #f1f1f1;
        }

        /* 手写 clearfix */
        .clearfix:after {
            content: '';
            display: table;
            clear: both;
        }
    </style>
</head>
<body>
    <div id="header">this is header</div>
    <div id="container" class="clearfix">
        <div id="center" class="column">this is center</div>
        <div id="left" class="column">this is left</div>
        <div id="right" class="column">this is right</div>
    </div>
    <div id="footer">this is footer</div>
</body>
</html>
```

* 双飞翼布局
```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>双飞翼布局</title>
    <style type="text/css">
        body {
            min-width: 550px;
        }
        .col {
            float: left;
        }
        #main {
            width: 100%;
            height: 200px;
            background-color: #ccc;
        }
        #main-wrap {
            margin: 0 190px 0 190px;
        }
        #left {
            width: 190px;
            height: 200px;
            background-color: #0000FF;
            margin-left: -100%;
        }
        #right {
            width: 190px;
            height: 200px;
            background-color: #FF0000;
            margin-left: -190px;
        }
    </style>
</head>
<body>
    <div id="main" class="col">
        <div id="main-wrap">
            this is main
        </div>
    </div>
    <div id="left" class="col">
        this is left
    </div>
    <div id="right" class="col">
        this is right
    </div>
</body>
</html>
```

#### 手写 clearfix

* 手写 clearfix
```css
/* 手写 clearfix */
.clearfix:after {
    content: '';
    display: table;
    clear: both;
}
```

### flex画骰子

## 定位
### absolute 和 relative 分别依据什么定位？
### 居中对齐有哪些方式？

## 图文样式
### line-height 的继承问题

## 响应式
### rem 是什么？
### 如何实现响应式？

## CSS3
### 关于 CSS3 动画