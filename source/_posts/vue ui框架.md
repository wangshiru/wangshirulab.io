---
title: vue ui框架
date: 2020-09-04 17:14:30
tags: [vue]
---

[参考](https://www.jianshu.com/p/1e05c8d68407)

# PC端ui框架
## Ant Design
[官网 Ant Design Vue](https://www.antdv.com/docs/vue/introduce-cn/)

## iVew
[官网](https://iviewui.com/)

## Element UI(未使用过，下个项目尝试)
[官网](https://element.eleme.cn/#/zh-CN)

## Vue Baidu Map
[官网](https://dafrok.github.io/vue-baidu-map)

# 移动端ui框架
## vant
[官网](https://github.com/youzan/vant)

## Muse-UI
[官网](https://muse-ui.org/#/zh-CN)

## VUX
[官网](https://doc.vux.li/zh-CN/components/qrcode.html)
样式很像微信

## Mint ui
[官网](http://mint-ui.github.io/docs/#/zh-cn2)

## vue-ydui
个人维护，自己练手可以使用，公司项目不建议使用
[官网](http://vue.ydui.org/docs/#/quickstart)

## Mand-Mobile
面向金融场景的Vue移动端UI组件库，丰富、灵活、实用，快速搭建优质的金融类产品。
[官网](https://github.com/didi/mand-mobile/tree/master)
