---
title: Yeoman基础
date: 2021-04-08 15:32:26
tags: [工具]
---
# 安装

```
npm install -g yo
```

# 使用

## 生成

```
MacBook-Pro gulp-demo$ yo --version
3.1.1
MacBook-Pro gulp-demo$ npm install generator-generator

added 1168 packages, and audited 1854 packages in 1m

81 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
MacBook-Pro gulp-demo$ yo generator
? Your generator name generator-wangshiru-gulp
Your generator must be inside a folder named generator-wangshiru-gulp
I'll automatically create this folder.
? Description this is a simple gulp project
? Project homepage url 
? Author's Name wangshiru
? Author's Email wangshrl@163.com
? Author's Homepage wangshiru.gitlab.io
? Package keywords (comma to split) gulp
? Send coverage reports to coveralls Yes
? Enter Node versions (comma separated) 15.4.0
? GitHub username or organization wangshiru
? Which license do you want to use? MIT
   create package.json
   create README.md
   create .editorconfig
   create .gitattributes
   create .gitignore
   create generators/app/index.js
   create generators/app/templates/dummyfile.txt
   create __tests__/app.js
   create .travis.yml
   create .eslintignore
   create LICENSE


I'm all done. Running npm install for you to install the required dependencies. If this fails, try running the command yourself.


npm WARN deprecated urix@0.1.0: Please see https://github.com/lydell/urix#deprecated
npm WARN deprecated resolve-url@0.2.1: https://github.com/lydell/resolve-url#deprecated
npm WARN deprecated samsam@1.3.0: This package has been deprecated in favour of @sinonjs/samsam
npm WARN deprecated har-validator@5.1.5: this library is no longer supported
npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
npm WARN deprecated request-promise-native@1.0.9: request-promise-native has been deprecated because it extends the now deprecated request package, see https://github.com/request/request/issues/3142

added 1140 packages, and audited 1140 packages in 2m

42 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
Thanks for using Yeoman.
- Enable Travis integration at https://travis-ci.com/profile/wangshiru
- Enable Coveralls integration at https://coveralls.io/repos/new
MacBook-Pro gulp-demo$ 
```

## 修改配置文件

然后修改generator目录中templates下的index.js

```js
'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(
        `Welcome to the marvelous ${chalk.red('generator-wsr-gulp')} generator!`
      )
    );

    const prompts = [
      {
        type: 'confirm',
        name: 'install',
        message: 'Would you like to enable this option?',
        default: true
      }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {
    this.fs.copy(
        // 需要修改的部分
        this.templatePath('**'),
        this.destinationPath('./')
    );
  }

  install() {
    this.installDependencies()
  }
};


```

## 本地测试

修改完之后，进行下述操作，进行本地测试

```
MacBook-Pro gulp-demo$ cd generator-wsr-gulp/
MacBook-Pro generator-wsr-gulp$ npm link

added 1 package in 2s
MacBook-Pro generator-wsr-gulp$ cd ../
MacBook-Pro gulp-demo$ mkdir demo-dest
MacBook-Pro gulp-demo$ cd demo-dest/
MacBook-Pro demo-dest$ yo wsr-gulp

     _-----_     ╭──────────────────────────╮
    |       |    │ Welcome to the marvelous │
    |--(o)--|    │    generator-wsr-gulp    │
   `---------´   │        generator!        │
    ( _´U`_ )    ╰──────────────────────────╯
    /___A___\   /
     |  ~  |     
   __'.___.'__   
 ´   `  |° ´ Y ` 

? Would you like to enable this option? Yes
   create package.json
   create gulpfile.js
   create index.html
   create css/index.scss
   create js/index.js


I'm all done. Running npm install && bower install for you to install the required dependencies. 
```

## 线上部署

```
MacBook-Pro demo-dest$ nrm ls

* npm -------- https://registry.npmjs.org/
  yarn ------- https://registry.yarnpkg.com/
  cnpm ------- http://r.cnpmjs.org/
  taobao ----- https://registry.npm.taobao.org/
  nj --------- https://registry.nodejitsu.com/
  npmMirror -- https://skimdb.npmjs.com/registry/
  edunpm ----- http://registry.enpmjs.org/

MacBook-Pro demo-dest$ npm login
npm notice Log in on https://registry.npmjs.org/
Username: wangshiru
Password: 
Email: (this IS public) wangshrl@163.com
Logged in as wangshiru on https://registry.npmjs.org/.
MacBook-Pro demo-dest$ cd ../
MacBook-Pro gulp-demo$ cd generator-wangshiru-gulp/
MacBook-Pro generator-wangshiru-gulp$ npm publish
npm notice 
npm notice 📦  generator-wangshiru-gulp@1.0.1
npm notice === Tarball Contents === 
npm notice 1.1kB   LICENSE                                
npm notice 1.5kB   README.md                              
npm notice 836B    generators/app/index.js                
npm notice 47B     generators/app/templates/css/index.scss
npm notice 1.5kB   generators/app/templates/gulpfile.js   
npm notice 185B    generators/app/templates/index.html    
npm notice 26B     generators/app/templates/js/index.js   
npm notice 532B    generators/app/templates/package.json  
npm notice 357.1kB generators/app/templates/yarn.lock     
npm notice 1.5kB   package.json                           
npm notice === Tarball Details === 
npm notice name:          generator-wangshiru-gulp                
npm notice version:       1.0.1                                   
npm notice filename:      generator-wangshiru-gulp-1.0.1.tgz      
npm notice package size:  100.6 kB                                
npm notice unpacked size: 364.3 kB                                
npm notice shasum:        5290229a3ede6c7c98d316c7a675e86f661cd7cc
npm notice integrity:     sha512-AhwNpqHuQfval[...]fRudJvVmA8bkQ==
npm notice total files:   10                                      
npm notice 
+ generator-wangshiru-gulp@1.0.1
MacBook-Pro generator-wangshiru-gulp$ 
```