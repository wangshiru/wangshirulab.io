---
title: vue生命周期
date: 2020-08-05 17:17:35
tags: [vue]
---

# created
实例已经创建，但数据还未渲染到页面模板

```js
var app = new vue({
    el: '#app',
    data: {
        msg: '测试'
    },
    created: function(){
        console.log('实例已经创建，但数据还未渲染到页面模板')
    }
})
```

# mounted
数据已经挂载到页面模板

# updated
实例更新之后

```js
setTimeout(function(){
    app.msg = 4000
},3000)
```

# destroyed
实例销毁了

```js
app.$destroy
```