---
title: cesium更改entity颜色
date: 2020-05-25 11:19:58
tags: [Cesium.js]
---
# 更改entity颜色
```js
$.each(china_map_entities._children,function(i,obj){          
    if(viewer.entities.getById(obj.id).polygon){
        viewer.entities.getById(obj.id).polygon.material=Cesium.Color.GREEN.withAlpha(0.3)
    }
    if(viewer.entities.getById(obj.id).polyline){
        viewer.entities.getById(obj.id).polyline.material=Cesium.Color.WHITE.withAlpha(0.1)
    }
})
```

[参考博客](https://blog.csdn.net/zlx312/article/details/79944769)