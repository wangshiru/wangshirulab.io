---
title: echarts基本图表运用
date: 2020-05-25 11:41:25
tags: [Echarts.js]
---
# 通用设置
[参考](https://echarts.apache.org/zh/option.html#title)

根据后端返回数据，动态生成不确定数目的折线图

```js
WebSocket.onmessage = function(event){
    var data = $.parseJSON(event.data)
    xData = data.time
    legendData = []
    seriesData = []
    for(i in data){
        legenddata.push(data[i].name)
        seriesData.push({
            name: data[i].name,
                    type:'line',
                    smooth: true,
                    data: data[i].value,
            itemStyle:{
                color: linecolor[i]
            }
        })
    }
    drawChart()
}
```

上述代码，由于push操作过于频繁，可能导致页面卡顿，所以数据计算尽量放在后端，定义接口时尽量方便渲染，避免计算。

```js
function drawChart(){
    var chart = echarts.init(document.getElementById('chart'));
    var  option = {
        legend: {
            top: '10px',
            right: '10px',
            textStyle: {
                color: '#fff'
            },
            data: legendData
        },
        // 四周边距(单位默认px，可以使用百分比)
        grid:{
            left: '10%',
            top: '15%',
            right: 50,
            bottom:'10%'
        },
        // 鼠标悬浮显示数据
        tooltip : {
            trigger: 'axis',
            axisPointer : {            
                type : 'shadow'       
             }
        },
        xAxis: [{
            type : 'category',
            // 设置轴线的属性
            axisLine:{
                onZero: false,
                lineStyle:{
                    color:'#fff',
                 }
            },
            splitLine: {
                show: false, 
                // 改变轴线颜色
                lineStyle:{
                    color:'#fff',
                },
                interval: 100                         
            },
            data: xData
        }],
        yAxis : [{
            type : 'value',
            // 控制网格线是否显示
            splitLine: {
                    show: false, 
                    改变轴线颜色
                    lineStyle:{
                    color:'#fff',
                }                           
            },
            // 设置轴线的属性
            axisLine:{
                onZero: false,
                lineStyle:{
                color:'#fff',
                }
            },
            axisLabel:{
                textStyle:{
                    fontSize: '1rem',
                    left:50,
                    
                },
                color: "#fff"
            },
            // 设置y轴最大值最小值
            min: 1.000,
            max: 1.035
        }],
        series: seriesData
    }
    chart.setOption(option)
    chart.resize()
    chart.hideLoading()
    // 自动适应屏幕浏览器的大小 当浏览器大小改变时 报表画布也跟着改变
    window.onresize = function () {
        chart.resize()
    }
}
```

# echarts插件

类似标签云的基于echarts的插件，配置和echarts格式相同，详见文档。

[ECharts wordcloud](https://github.com/ecomfe/echarts-wordcloud)

```js
series: [{
   type: 'wordCloud',
        gridSize: 2,
        sizeRange: [12, 50],
        rotationRange: [-90, 90],
        shape: 'pentagon',
        textStyle: {
            normal: {
                color: function () {
                    return 'rgb(' + [
                            Math.round(Math.random() * 255),
                            Math.round(Math.random() * 255),
                            Math.round(Math.random() * 255)
                        ].join(',') + ')';
                }
            },
            emphasis: {
                shadowBlur: 10,
                shadowColor: '#333'
            }
        },
        data: [
            {
                name: 'Sam S Club',
                value: 10000,
            }, {
                name: 'Macys',
                value: 6181
            }
        ]
}]
```