---
title: cesium更改图层颜色属性
date: 2020-06-03 18:37:33
tags: [Cesium.js]
top: 2
---

# 问题描述
1. 本地部署完谷歌影像之后发现地图颜色过绿，不符合我们想要的蓝色系，导致整个页面色系不搭，需要调整影像图层的颜色属性。
2. 修改默认背景，增加天空盒。

# 解决
## 问题1解决

```js
var imageryProvider= new Cesium.TileMapServiceImageryProvider({
    url: 'http://xxx/xxx'
})

// defaultBrightness 默认亮度。1.0使用未修改的图像颜色。小于1.0会使图像变暗，而大于1.0会使图像变亮。
imageryProvider.defaultBrightness = 0.5

// 默认对比度。1.0使用未修改的图像颜色。小于1.0会降低对比度，而大于1.0则会提高对比度。
imageryProvider.defaultContrast = 2

// 默认色调（以弧度为单位）。0.0使用未修改的图像颜色。
imageryProvider.defaultHue = 0

// 默认饱和度。1.0使用未修改的图像颜色。小于1.0会降低饱和度，而大于1.0则会增加饱和度。
imageryProvider.defaultSaturation = 0.5

// 默认伽玛校正。1.0使用未修改的图像颜色。
imageryProvider.defaultGamma = 3
```

## 问题2解决

```js
var scene=viewer.scene
scene.skyBox = new Cesium.SkyBox({
    sources : {
        positiveX : 'img_px.jpg',
        negativeX : 'img_nx.jpg',
        positiveY : 'img_ny.jpg',
        negativeY : 'img_py.jpg',
        positiveZ : 'img_pz.jpg',
        negativeZ : 'img_nz.jpg'
    }
})
```
