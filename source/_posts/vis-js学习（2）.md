---
title: vis.js学习（2）
date: 2019-04-02 13:36:32
tags: [关系图]
---
# 需求
设计节点的实体及关系样式，如果在json中设置，会发现出现大量的重复的数据，所以把每个实体类型单独拿出来作为组(group)

# 代码

```js
var options = {
    nodes: {
      shape: 'dot',
      font: {
        face: 'Tahoma'
      },
      borderWidth:1.5,
      scaling:{
        label: {
          min: 1,
          max: 5
        }
      },
      font: {size:12, color:'#626568'}
    },
    edges: {
      width: 0.2,
      smooth: {
        type: 'continuous'
      },
      arrows: "to",
    },
    groups: {
      Person: {
        shape: "circularImage",
        image: "./images/Person.png",
        color: {
          background: "#fff",
          border: "#022240", 
          highlight: {
            "border": "#d70007",
            "background": "#fec40a"
          }
        }
      },
      Woman: {
        shape: "circularImage",
        image: "./images/Woman.png",
        color: {
          background: "#fff",
          border: "#022240", 
          highlight: {
            "border": "#d70007",
            "background": "#fec40a"
          }
        }
      },
      Insurgent: {
        shape: "circularImage",
        image: "./images/Insurgent.png",
        color: {
          background: "#fff",
          border: "#022240", 
          highlight: {
            "border": "#d70007",
            "background": "#fec40a"
          }
        }
      }
    },
    interaction: {
    },
    physics: false
};
```
